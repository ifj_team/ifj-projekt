/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


/*
 * INCLUDE (MACRO) GUARD 
 */
#ifndef INTERPRET
#define INTERPRET

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "errorhandler.h"
#include "ial.h"
#include "str.h"
#include "instlist.h"
#include "parser.h"

/**
 * Function of interpret.c
 * @Interpret manages all activities of Interpret
 * @InstListSubstitute substitute of Instructions List
 */
bool Interpret( tInstList * );
tInstList* InstListSubstitute( tInstList *, TSymTable * );

#endif