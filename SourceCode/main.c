/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include "ial.h"
#include "instlist.h"
#include "errorhandler.h"
#include "scanner.h"
#include "parser.h"
#include "interpret.h"

/* size of hash table */
#define T_SIZE 3331

/* line counter (scanner.c) */
extern int LINE_CNT;

/**
 * @DECLARATIONS
 * Source file
 * Hash table
 * Error handler
 * Instruction List
 * Token
 */
FILE *source;
TSymTable *sTable;
tErrorHandler error;
tInstList ilist;
token ParsToken;
string str;

/* ERROR FLAG */
int ERR_FLAG = E_OK;

/**
 * @desc Validating command line arguments and opening propper file
 * @param argc number of params (from command line)
 * @param argv adresses of params (from command line)
 * @return error report
 */
int ProcessParams( int argc, char **argv ) {

    /* We accept only 1 user argument (etc. ./compiler file) */
    if (argc == 1) { 
	/* ERROR 99, INTERNAL ERROR */
	ErrorAdd(&error,LINE_CNT,E_INTERNAL);
        return ERR_FLAG;
    }
    
    /* error while opening file */
    if ((source = fopen(argv[1],"r")) == NULL) { 
	/* ERROR 99, INTERNAL ERROR */
	ErrorAdd(&error,LINE_CNT,E_INTERNAL);
	return ERR_FLAG;
    }
    /* WITHOUT ERROR */
    return ERR_FLAG;
}

int main(int argc, char **argv) {
    
    /** 
    * @Initializations 
    * Instruction list
    * Hash table
    * Token
    * Error Handler
    */
    InstListInit(&ilist);
    sTable = InitTab(T_SIZE);
    ErrorInit(&error);
    
    if( !ERR_FLAG ) {
	if( !ProcessParams(argc,argv) ) {
	    /* set source file for scanner */
	    SetSourceFile(source);
	    /* if parser succeeded executing interpret */
	    if( !Parse() ) {
		Interpret(&ilist);
	    }
	}
    }
    else 
	/* problem with inicializations */
	ErrorAdd(&error,LINE_CNT,E_INTERNAL);
    
    ErrorPrint(); 
    /* close file */
    if( source ) fclose(source);
    /* free memory */
    InstListDispose(&ilist);
    DeleteTable(sTable);

    return ERR_FLAG;
}
