/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01   <<<<<<                              *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#include "scanner.h"

extern FILE *source;
extern token ParsToken;
int LINE_CNT = 1;

/*
 * @desc - saves name of input file into global variable
 * @param FILE *f - input file
 *
 */
void SetSourceFile(FILE *f) {
   source = f;
}

/* 
 * @desc - Determine type of token according to input. All identifiers are converted into lowercase,
		   so we can compare strings (case insensitive). It's necessary to save all key words in table of symbols
		   in lowercase.
 * @param string *attr - for loading the character from the input for the given token
 * @return - returns type of token according to module scanner.h or lexical error
 */
int GetToken(string *attr) {
	int state = 0;
	int c; // character
	int num; // number from string literal (#010)
	ParsToken.tok = FETCH; // no token recognized yet

	// removes atributs value for new string
	strClear(attr);
	while (1) {
		c = getc(source);

		switch (state) {
			case 0:
				if (isspace(c)) {
				   // white space
					if (c == '\n')
						LINE_CNT++;
					state = 0;
				}
				else if (c == '{') {
					// comment
					state = 1;
				}
				else if (isalpha(c) || c == '_') {
					// ID or key word
					if (c != '_')
						c = tolower(c);
					strAddChar(attr, c);
					state = 2;
				}
				else if (isdigit(c)) {
					// Decimal number or integer
					strAddChar(attr, c);
					state = 3;
				}
				else if (c == APOS) {
					// string literal ... APOS == ASCII value of apostrophe (')
					state = 4;
				}
				else if (c == ':') {
					// := or :
					state = 5;
				}
				else if (c == '<') {
					// < or <= or <>
					state = 6;
				}
				else if (c == '>') {
					// < or >=
					state = 7;
				}
				else if (c == '=') {
					ParsToken.tok = EQ;
					return E_OK;
				}
				else if (c == ';') {
					ParsToken.tok = SEMICOL;
					return E_OK;
				}
				else if (c == '(') {
					ParsToken.tok = LBR;
					return E_OK;
				}
				else if (c == ')') {
					ParsToken.tok = RBR;
					return E_OK;
				}
				else if (c == ',') {
					ParsToken.tok = COMMA;
					return E_OK;
				}
				else if (c == '+') {
					ParsToken.tok = PLUS;
					return E_OK;
				}
				else if (c == '-') {
					ParsToken.tok = MINUS;
					return E_OK;
				}
				else if (c == '*') {
					ParsToken.tok = MULT;
					return E_OK;
				}
				else if (c == '/') {
					ParsToken.tok = DIV;
					return E_OK;
				}
				else if (c == EOF) {
					ParsToken.tok = ENDFILE;
					return E_OK;
				}
				else 
					return E_LEXICAL;
			   break;

			case 1:
				// commentary
				if (c == '}')
					state = 0;
				else if (c == EOF)
					return E_LEXICAL;
			   break;

			case 2:
				// ID or key word
				if (isalnum(c) || c == '_') {
					if (isalpha(c))
						c = tolower(c);
					strAddChar(attr, c);
				}
				else {
					// it's necessary to give back last character
					ungetc(c, source);
					// check if ID == key word
					if (strCmpConstStr(attr, "begin") == 0) {
						ParsToken.tok = BEGIN;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "boolean") == 0) {
						ParsToken.tok = BOOLEAN;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "do") == 0) {
						ParsToken.tok = DO;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "else") == 0) {
						ParsToken.tok = ELSE;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "end") == 0) {
						if ((c = getc(source)) == '.') {
							strAddChar(attr, c);
							ParsToken.tok = ENDDOT;
							return E_OK;
						}
						else {
							ungetc(c, source);
							ParsToken.tok = END;
							return E_OK;
						}
					}
					else if (strCmpConstStr(attr, "false") == 0) {
						ParsToken.tok = FFALSE;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "find") == 0) {
						ParsToken.tok = FIND;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "forward") == 0) {
						ParsToken.tok = FORWARD;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "function") == 0) {
						ParsToken.tok = FUNCTION;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "if") == 0) {
						ParsToken.tok = IF;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "integer") == 0) {
						ParsToken.tok = INTEGER;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "readln") == 0) {
						ParsToken.tok = READLN;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "real") == 0) {
						ParsToken.tok = REAL;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "sort") == 0) {
						ParsToken.tok = SORT;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "string") == 0) {
						ParsToken.tok = STRING;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "then") == 0) {
						ParsToken.tok = THEN;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "true") == 0) {
						ParsToken.tok = TTRUE;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "var") == 0) {
						ParsToken.tok = VAR;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "while") == 0) {
						ParsToken.tok = WHILE;
						return E_OK;
					}
					else if (strCmpConstStr(attr, "write") == 0) {
						ParsToken.tok = WRITE;
						return E_OK;
					}
					else {
						ParsToken.tok = ID;
						strCopyString(&ParsToken.key, attr);
						return E_OK;
					}
				}
				break;

			case 3:
				// decimal number or integer
				if (isdigit(c)) {
					strAddChar(attr, c);
				}
				else if (c == '.') {
					strAddChar(attr, c);
					state = 11;
				}
				else if (c == 'e' || c == 'E') {
					strAddChar(attr, c);
					state = 12;
				}
				else {
					ungetc(c, source);
					if (numStrToI(attr, &ParsToken.inum) != E_OK)
						return E_SEMANTIC_EXP;
					else {
						ParsToken.tok = INT;
						return E_OK;
					}
				}
				break;

			case 4:
				// string literal
				num = 0;
				if (c == APOS) {
					state = 8;
				}
				else if (c == EOF || c == NL)
					return E_LEXICAL;
				else
					strAddChar(attr, c);
				break;

			case 5:
				// := or :
				if (c == '=') {
					ParsToken.tok = ASSIGN;
					return E_OK;
				}
				else {
					ungetc(c, source);
					ParsToken.tok = COLON;
					return E_OK;
				}
				break;

			case 6:
				// < or <= or <>
				if (c == '=') {
					ParsToken.tok = SMLLEQ;
					return E_OK;
				}
				if (c == '>') {
					ParsToken.tok = DIFF;
					return E_OK;
				}
				else {
					ungetc(c, source);
					ParsToken.tok = SMLL;
					return E_OK;
				}
				break;

			case 7:
				// > or >=
				if (c == '=') {
					ParsToken.tok = BGGEQ;
					return E_OK;
				}
				else {
					ungetc(c, source);
					ParsToken.tok = BGG;
					return E_OK;
				}
				break;

			case 8:
				// continuation of string literal
				if (c == APOS) {
					strAddChar(attr, c);
					state = 4;
				}
				else if (c == '#') {
					state = 9;
				}
				else {
					ungetc(c, source);
					strCopyString(&ParsToken.key, attr);
					ParsToken.tok = STR;
					return E_OK;
				}
				break;

			case 9:
				// continuation of string literal
				if (isdigit(c)) {
					num = num * 10 + (c - '0');
					state = 10;
				}
				else
					return E_LEXICAL;
				break;

			case 10:
				// continuation of string literal
				if (isdigit(c)) {
					if ((num = num * 10 + (c - '0')) > 255)
						return E_LEXICAL;
				}
				else if (c == APOS) {
					// Check if number is in interval <1; 255>
					if (num >= 1 && num <= 255) {
						strAddChar(attr, num);
						state = 4;
					}
					else {
						return E_LEXICAL;
					}
				}
				else
					return E_LEXICAL;
				break;

			case 11:
				// continuation of decimal number
				if (isdigit(c)) {
					strAddChar(attr, c);
					state = 13;
				}
				else
					return E_LEXICAL;
				break;

			case 12:
				// continuation of decimal number
				if (isdigit(c)) {
					strAddChar(attr, c);
					state = 15;
				}
				else if (c == '+' || c == '-') {
					strAddChar(attr, c);
					state = 14;
				}
				else
					return E_LEXICAL;
				break;

			case 13:
				// continuation of decimal number
				if (isdigit(c)) {
					strAddChar(attr, c);
				}
				else if (c == 'e' || c == 'E') {
					strAddChar(attr, c);
					state = 12;
				}
				else {
					ungetc(c, source);
					if (numStrToD(attr, &ParsToken.dnum) != E_OK)
						return E_SEMANTIC_EXP;
					else {
						ParsToken.tok = DOUBLE;
						return E_OK;
					}
				}
				break;

			case 14:
				// continuation of decimal number
				if (isdigit(c)) {
					strAddChar(attr, c);
					state = 15;
				}
				else
					return E_LEXICAL;
				break;

			case 15:
				// continuation of decimal number
				if (isdigit(c)) {
					strAddChar(attr, c);
				}
				else {
					ungetc(c, source);
					if (numStrToD(attr, &ParsToken.dnum) != E_OK)
						return E_SEMANTIC_EXP;
					else {
						ParsToken.tok = DOUBLE;
						return E_OK;
					}				
				}
				break;
		}
	}
}

/* USAGE EXAMPLE
int main() {
	FILE *f;
	f = fopen(argv[1], "r");
	SetSourceFile(f);


	string s;
	if (strInit(&s) != E_OK)
		ret chyba

	token ParsToken;
	if (strInit(&ParsToken.key) != E_OK)
		ret chyba

	while (GetToken(&s) == E_OK && ParsToken.tok != ENDFILE)

	strFree(&s);
	strFree(&ParsToken.key);
	fclose(f);
}
*/
