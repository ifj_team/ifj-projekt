/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


/*
 * INCLUDE (MACRO) GUARD 
 */
#ifndef INSTRUCTION_LIST
#define INSTRUCTION_LIST

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "errorhandler.h"
#include "ial.h"

/**
 * INSTRUCTIONS LIST
 * -----------------
 * Instruction 0,
 * I_STOP: 		@desc Ending Instruction END. 
 * 			@usage InsertInstruction(&PtrToInstList, I_STOP, NULL, NULL, NULL)
 * -------------------------------------------------------------------------------
 * Instruction 1,
 * I_READ:		@desc Read
 * 			@usage InsertInstruction(&PtrToInstList, I_READ, NULL, NULL, dest)
 * 			dest - Where we read
 * -------------------------------------------------------------------------------
 * Instruction 2,
 * I_WRITE:		@desc Write
 * 			@usage InsertInstruction(&PtrToInstList, I_WRITE, src1, NULL, NULL)
 * 			src1 - What we write
 * --------------------------------------------------------------------------------
 * Instruction 3,
 * I_ADD:		@desc addition for real and integer, concetation for strings
 * 			@usage InsertInstruction(&PtrToInstList, I_ADD, src1, src2, dest)
 * 			src1, src2 - operand
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 4,
 * I_SUB:		@desc substraction for real and integer
 * 			@usage InsertInstruction(&PtrToInstList, I_SUB, src1, src2, dest)
 * 			src1, src2 - operand
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 5,
 * I_MUL:		@desc multiply for real and integer
 * 			@usage InsertInstruction(&PtrToInstList, I_MUL, src1, src2, dest)
 * 			src1, src2 - operand
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 6,
 * I_DIV:		@desc division for real and integer
 * 			@usage InsertInstruction(&PtrToInstList, I_DIV, src1, src2, dest)
 * 			src1 - dividend
 * 			src2 - divisor
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 7,
 * I_NOT:		@desc negation
 * 			@usage InsertInstruction(&PtrToInstList, I_NOT, src1, NULL, NULL)
 * 			src1 - value
 * ------------------------------------------------------------------------------
 * Instruction 8,
 * I_LESS:		@desc less than "<"
 * 			@usage InsertInstruction(&PtrToInstList, I_LESS, src1, src2, dest)
 * 			src1, src2 - values
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 9,
 * I_LESS_EQL:		@desc less than or equal "<="
 * 			@usage InsertInstruction(&PtrToInstList, I_LESS_EQL, src1, src2, dest)
 * 			src1, src2 - values
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 10,
 * I_GREATER:		@desc greater than ">"
 * 			@usage InsertInstruction(&PtrToInstList, I_GREATER, src1, src2, dest)
 * 			src1, src2 - values
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 11,
 * I_GREATER_EQL:	@desc greater than or equal ">="
 * 			@usage InsertInstruction(&PtrToInstList, I_GREATER_EQL, src1, src2, dest)
 * 			src1, src2 - operand
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 12,
 * I_EQL:		@desc equal "=="
 * 			@usage InsertInstruction(&PtrToInstList, I_EQL, src1, src2, dest)
 * 			src1, src2 - values
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 13,
 * I_DIFF:		@desc different "<>"
 * 			@usage InsertInstruction(&PtrToInstList, I_DIFF, src1, src2, dest)
 * 			src1, src2 - operand
 * 			dest - result
 * ------------------------------------------------------------------------------
 * Instruction 14,
 * I_FIND:		@desc use function int BMA( char *, char *)
 * 			@usage InsertInstruction(&PtrToInstList, I_FIND, src1, src2, dest)
 * 			src1 - pattern
 * 			src2 - text
 * 			dest - index to text
 * ------------------------------------------------------------------------------
 * Instruction 15,
 * I_SORT:		@desc use function char* QickSort( string *)
 * 			@usage InsertInstruction(&PtrToInstList, I_SORT, src1, NULL, dest)
 * 			src1 - array for sorting
 * 			dest - sorted array
 * ------------------------------------------------------------------------------
 * Instruction 16,
 * I_LEN:		@desc length of string
 * 			@usage InsertInstruction(&PtrToInstList, I_LEN, src1, src2, dest)
 * 			src1 - string
 * 			dest - length
 * ------------------------------------------------------------------------------
 * Instruction 17,
 * I_COPY:		@desc use function string* strSub( string* , int, int )
 * 			@usage 	InsertInstruction(&PtrToInstList, I_COPY, src1, src2, NULL)
 * 				src1 - index
 * 				src2 - counted
 * 				InsertInstruction(&PtrToInstList, I_COPY, src1, NULL, dest)
 * 				src1 - string
 * 				dest - substring
 * ------------------------------------------------------------------------------
 * Instruction 18,
 * I_LAB:		@desc label to jump
 * 			@usage InsertInstruction(&PtrToInstList, I_LAB, NULL, NULL, NULL)
 * ------------------------------------------------------------------------------
 * Instruction 19,
 * I_JUMP:		@desc jump to instruction
 * 			@usage InsertInstruction(&PtrToInstList, I_JUMP, src1/NULL, NULL, dest)
 * 			if src1 != NULL is conditional jump
 * 			src1 - condition
 * 			dest - address of the jump
 * ------------------------------------------------------------------------------
 * Instruction 20,
 * I_MOV:		@desc assignment ":="
 * 			@usage InsertInstruction(&PtrToInstList, I_MOV, src1, NULL, dest)
 * 			src1 - value
 * 			dest - variable
 * ------------------------------------------------------------------------------
 * Instruction 21,
 * I_FRAME:		@desc create frame
 * 			@usage InsertInstruction(&PtrToInstList, I_FRAME, src1, NULL, dest)
 * 			src1 - pointer to local table
 * 			dest - pointer to copy of local table
 * ------------------------------------------------------------------------------
 * Instruction 22,
 * I_CIL:		@desc copy Instuctions List
 * 			@usage InsertInstruction(&PtrToInstList, I_CIL, src1, NULL, dest)
 * 			src1 - pointer to Instructions list
 * 			dest - pointer to copy of Instructions list
 */
typedef enum {
  I_STOP = 0,
  I_READ = 1,
  I_WRITE = 2,
  I_ADD = 3,
  I_SUB = 4,
  I_MUL = 5,
  I_DIV = 6,
  I_NOT = 7,
  I_LESS = 8,
  I_LESS_EQL = 9,
  I_GREATER = 10,
  I_GREATER_EQL = 11,
  I_EQL = 12,
  I_DIFF = 13,
  I_FIND = 14,
  I_SORT = 15,
  I_LEN = 16,
  I_COPY = 17,
  I_LAB = 18,
  I_JUMP = 19,
  I_MOV = 20,
  I_FRAME = 21,
  I_CIL = 22,
  I_ILS = 23,
  I_SET = 24,
  I_RET = 25,
  I_REC = 26,

} iInstruction;  

/**
 * @desc 3 address code
 */
typedef struct tInst
{
  iInstruction type;  
  struct TSynonym *src1; 
  struct TSynonym *src2; 
  struct TSynonym *dest; 
  struct tInst *nextItem;
} *tInstPtr;
    
typedef struct tInstList
{
  struct tInst *first;  
  struct tInst *last;   
  struct tInst *active; 
} tInstList;

/**
 * instlist functions
 * @function InstListInit, initialization, instructions list
 * @function InstListDispose, free allocated memory of instructions list
 * @function InsertInstruction, add new instruction to Instruction list as the last item

 * @function InstListActive, test activity
 * @funstion InstListSetFirst, set first item as active item 
 * @function InstListSucc, shift activity to the next item

 * @function InstListCopy, return pointer to copy of instruction list

 */

void InstListInit( tInstList * );
void InstListDispose( tInstList * );
int InsertInstruction( tInstList *, iInstruction type, struct TSynonym *, struct TSynonym *, struct TSynonym * );

bool InstListActive( tInstList * );
void InstListSetFirst( tInstList * );
void InstListSucc( tInstList * );

tInstList* InstListCopy( tInstList * );

#endif
