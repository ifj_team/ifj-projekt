/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05   <<<<<<                              *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/



#include "parser.h"

/**
  * @desc Global variables
  */

extern TSymTable *sTable;
extern tInstList ilist;
extern token ParsToken;
extern int LINE_CNT; //Line counter from scanner
extern tErrorHandler error;
extern int ERR_FLAG;

TStack *stack;  //stack for local TS

string key;
string s;


/**
 * @desc find variable in global/local TS.
 * @param *hash - pointer to table where to search
 * @param index - index of finding parameter
 * @return errorValue
 */
TSynonym *FindParam (TSymTable *hash, int index){
    TSynonym *tmp;

    for (int i = 0; i < hash->sizeOfTable; i++){
	tmp = hash->columns[i];
	while (tmp != NULL){
	    if ((tmp->paramCounter == index)) return tmp;
	    tmp = tmp->nextSynonym;
	}
    }
    return NULL;
}

/**
 * @desc find variable in global/local TS.
 * @param *Id - id of searching variable or function
 * @return errorValue
 */
TSynonym *FindVariable (char *Id){
    TSymTable *hash;
    TSynonym *tmp;

    if (!EmptyStack(stack)) {  //stack is not empty
	hash = TopStack(stack)->table;
	if (((tmp = SearchTable(hash, Id)) == NULL) && ((tmp = SearchTable(sTable, Id)) == NULL))
	    return NULL;  //no variable was found
    }
    //stack si empty -> search for variable only in global TS
    else tmp = SearchTable(sTable, Id);
    return tmp;
}

/**
 * @desc checking syntax of non-terminal <BuiltIn>
 * @param *ilist - list of instruction
 * @param *destination - where save result of function
 * @return errorValue
 */
int BuiltIn (tInstList *ilist, TSynonym *destination){
    int fce = ParsToken.tok;

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok == ASSIGN) return E_SEMANTIC_OTHER;
    if (ParsToken.tok != LBR) return E_SYNTACTIC;

    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    TSynonym *firstParam;
    TSynonym *secondParam;
    TSynonym *thirdParam;
    TDataValue value;
    switch (fce){
	case FIND:
	    if (destination->dataType != D_INT) return E_SEMANTIC_EXP;
	    if (GetToken(&s)) return E_LEXICAL;
	    //first term must be string or variable of type string
	    if (ParsToken.tok == STR) {
    		value.string = malloc(sizeof (string));
    		strInit(value.string);
		strCopyString(value.string, &ParsToken.key);
		if ((firstParam = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	firstParam->inicialized = true;
	    }
	    else if ((ParsToken.tok != ID) || ((firstParam = FindVariable(ParsToken.key.str)) == NULL) || (firstParam->dataType != D_STRING))
		return E_SEMANTIC_EXP;

	    if (GetToken(&s)) return E_LEXICAL;
	    //second term must be string or variable of type string
	    if (ParsToken.tok != COMMA) return E_SYNTACTIC;
	    if (GetToken(&s)) return E_LEXICAL;

	    if (ParsToken.tok == STR) {
    		value.string = malloc(sizeof (string));
    		strInit(value.string);
		strCopyString(value.string, &ParsToken.key);
		if ((secondParam = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	secondParam->inicialized = true;
	    }
	    else if ((ParsToken.tok != ID) || ((secondParam = FindVariable(ParsToken.key.str)) == NULL) || (secondParam->dataType != D_STRING))
		return E_SEMANTIC_EXP;

	    if (InsertInstruction(ilist, I_FIND, firstParam, secondParam, destination)) return E_INTERNAL;
	    break;
	case SORT:
	    if (destination->dataType != D_STRING) return E_SEMANTIC_EXP;
	    if (GetToken(&s)) return E_LEXICAL;
	    //term must be string or variable of type string
	    if (ParsToken.tok == STR) {
    		value.string = malloc(sizeof (string));
    		strInit(value.string);
		strCopyString(value.string, &ParsToken.key);
		if ((firstParam = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	firstParam->inicialized = true;
	    }
	    else if ((ParsToken.tok != ID) || ((firstParam = FindVariable(ParsToken.key.str)) == NULL) || (firstParam->dataType != D_STRING))
		return E_SEMANTIC_EXP;

	    if (InsertInstruction(ilist, I_SORT, firstParam, NULL, destination)) return E_INTERNAL;
	    break;
	case ID:
	    if (strcmp(ParsToken.key.str, "copy") == 0) {  //built-in function copy
		if (destination->dataType != D_STRING) return E_SEMANTIC_EXP;
		if (GetToken(&s)) return E_LEXICAL;
		//first term must be string or variable of type string
	    	if (ParsToken.tok == STR) {
    		    value.string = malloc(sizeof (string));
    		    strInit(value.string);
		    strCopyString(value.string, &ParsToken.key);
		    if ((firstParam = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	    firstParam->inicialized = true;
	    	}
		else if ((ParsToken.tok != ID) || ((firstParam = FindVariable(ParsToken.key.str)) == NULL) || (firstParam->dataType != D_STRING))
		    return E_SEMANTIC_EXP;

		if (GetToken(&s)) return E_LEXICAL;
	    	if (ParsToken.tok != COMMA) return E_SYNTACTIC;
	    	if (GetToken(&s)) return E_LEXICAL;

		//second term must be integer or variable of type integer
	    	if (ParsToken.tok == INT) {
		    value.integer = ParsToken.inum;
		    if ((secondParam = NewSynonym(hash, NULL, I_CONST, D_INT, value, true)) == NULL) return E_INTERNAL;
	    	    secondParam->inicialized = true;
	    	}
		else if ((ParsToken.tok != ID) || ((secondParam = FindVariable(ParsToken.key.str)) == NULL) || (secondParam->dataType != D_INT))
		    return E_SEMANTIC_EXP;

		if (GetToken(&s)) return E_LEXICAL;
	    	if (ParsToken.tok != COMMA) return E_SYNTACTIC;
	    	if (GetToken(&s)) return E_LEXICAL;

		//third term must be integer or variable of type integer
	    	if (ParsToken.tok == INT) {
		    value.integer = ParsToken.inum;
		    if ((thirdParam = NewSynonym(hash, NULL, I_CONST, D_INT, value, true)) == NULL) return E_INTERNAL;
	    	    thirdParam->inicialized = true;
	    	}
		else if ((ParsToken.tok != ID) || ((thirdParam = FindVariable(ParsToken.key.str)) == NULL) || (thirdParam->dataType != D_INT))
		    return E_SEMANTIC_EXP;

	    	if (InsertInstruction(ilist, I_COPY, secondParam, thirdParam, NULL)) return E_INTERNAL;
	    	if (InsertInstruction(ilist, I_COPY, firstParam, NULL, destination)) return E_INTERNAL;
		break;
	    }
	    else if (strcmp(ParsToken.key.str, "length") == 0) {  //built-in function length
		if (destination->dataType != D_INT) return E_SEMANTIC_EXP;
		if (GetToken(&s)) return E_LEXICAL;
		//term must be string or variable of type string
	    	if (ParsToken.tok == STR) {
    		    value.string = malloc(sizeof (string));
    		    strInit(value.string);
		    strCopyString(value.string, &ParsToken.key);
		    if ((firstParam = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	    firstParam->inicialized = true;
	    	}
		else if ((ParsToken.tok != ID) || ((firstParam = FindVariable(ParsToken.key.str)) == NULL) || (firstParam->dataType != D_STRING))
		    return E_SEMANTIC_EXP;

	    	if (InsertInstruction(ilist, I_LEN, firstParam, NULL, destination)) return E_INTERNAL;
	    }
	    else return E_SEMANTIC_EXP;
	    break;
	default:
	    return E_SYNTACTIC;
	    break;
    }

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != RBR) return E_SYNTACTIC;

    return E_OK;
}
/**
 * @desc checking syntax of non-terminal <Readln>
 * @param *ilist - list of instruction
 * @return errorValue
 */
int Readln (tInstList *ilist){  //<Readln> -> readln ( ID )

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok == ASSIGN) return E_SEMANTIC_OTHER;
    if (ParsToken.tok != LBR) return E_SYNTACTIC;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ID) return E_SYNTACTIC;

    TSynonym *tmp;
    if (((tmp = FindVariable(ParsToken.key.str)) == NULL) || (tmp->type != I_VAR)) return E_SEMANTIC_VAR;

    if (tmp->dataType == D_BOOL) return E_SEMANTIC_EXP;
    if (InsertInstruction(ilist, I_READ, NULL, NULL, tmp)) return E_INTERNAL;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != RBR) return E_SYNTACTIC;

    return E_OK;
}

/**
 * @desc checking syntax of non-terminal <Write>
 * @param *ilist - list of instruction
 * @return errorValue
 */
int Write (tInstList *ilist){  //<Write> -> write ( <WriteFirstTerm> <Term> )

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok == ASSIGN) return E_SEMANTIC_OTHER;
    if (ParsToken.tok != LBR) return E_SYNTACTIC;

    if (GetToken(&s)) return E_LEXICAL;

    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    TSynonym *tmp;
    TDataValue value;
    switch (ParsToken.tok){
	case DOUBLE:
	    value.real = ParsToken.dnum;
	    if ((tmp = NewSynonym(hash, NULL, I_CONST, D_REAL, value, true)) == NULL) return E_INTERNAL;
	    tmp->inicialized = true;
	    break;
	case INT:
	    value.integer = ParsToken.inum;
	    if ((tmp = NewSynonym(hash, NULL, I_CONST, D_INT, value, true)) == NULL) return E_INTERNAL;
	    tmp->inicialized = true;
	    break;
	case STR:
	    value.string = malloc(sizeof (string));
	    strInit(value.string);
	    strCopyString(value.string, &ParsToken.key);
	    if ((tmp = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    tmp->inicialized = true;
	    break;
	case TTRUE:
	    value.boolean = true;
	    if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
	    tmp->inicialized = true;
	    break;
	case FFALSE:
	    value.boolean = false;
	    if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
	    tmp->inicialized = true;
	    break;
	case ID:
	    if (((tmp = FindVariable(ParsToken.key.str)) == NULL) || (tmp->type != I_VAR)) return E_SEMANTIC_VAR;
	    break;
	default:
	    return E_SYNTACTIC;
	    break;
    }

    if (InsertInstruction(ilist, I_WRITE, tmp, NULL, NULL)) return E_INTERNAL;

    if (GetToken(&s)) return E_LEXICAL;

    while (ParsToken.tok == COMMA){
	if (GetToken(&s)) return E_LEXICAL;
	switch (ParsToken.tok){
	    case DOUBLE:
		value.real = ParsToken.dnum;
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_REAL, value, true)) == NULL) return E_INTERNAL;
	    	tmp->inicialized = true;
		break;
	    case INT:
	    	value.integer = ParsToken.inum;
	    	if ((tmp = NewSynonym(hash, NULL, I_CONST, D_INT, value, true)) == NULL) return E_INTERNAL;
	    	tmp->inicialized = true;
	    	break;
	    case STR:
	    	value.string = malloc(sizeof (string));
	    	strInit(value.string);
	    	strCopyString(value.string, &ParsToken.key);
	    	if ((tmp = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
	    	tmp->inicialized = true;
	    	break;
	    case TTRUE:
	    	value.boolean = true;
	    	if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
	    	tmp->inicialized = true;
	    	break;
	    case FFALSE:
	    	value.boolean = false;
	    	if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
	    	tmp->inicialized = true;
	    	break;
	    case ID:
		if (((tmp = FindVariable(ParsToken.key.str)) == NULL) || (tmp->type != I_VAR)) return E_SEMANTIC_VAR;
		break;
	    default:
		return E_SYNTACTIC;
		break;
	}
    	if (InsertInstruction(ilist, I_WRITE, tmp, NULL, NULL)) return E_INTERNAL;
	if (GetToken(&s)) return E_LEXICAL;
    }

    if (ParsToken.tok != RBR) return E_SYNTACTIC;

    return E_OK;
}

/**
 * @desc checking syntax of non-terminal <While>
 * @param *ilist - list of instruction
 * @return errorValue
 */
int While (tInstList *ilist){  //<While> -> while <Expression> do <CompoundStatement>
    int result;

    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    if (GetToken(&s)) return E_LEXICAL;
    TSynonym *synonym;  //save result of Expression()
    TDataValue value;
    TSynonym *labelExpr, *labelEndWhile;
    value.label = NULL;

    if ((labelExpr = NewSynonym(hash, NULL, I_LABEL, D_LABEL, value, false)) == NULL) return E_INTERNAL;
    if ((labelEndWhile = NewSynonym(hash, NULL, I_LABEL, D_LABEL, value, false)) == NULL) return E_INTERNAL;
    if (InsertInstruction(ilist, I_LAB, NULL, NULL, NULL)) return E_INTERNAL;  //create label
    labelExpr->value.label = ilist->last;  //save pointer to instruction in body of while

    if ((result = Expression(hash, &synonym, ilist)) != E_OK) return result;
    if (InsertInstruction(ilist, I_NOT, synonym, NULL, NULL)) return E_INTERNAL; //negate condition
    if (InsertInstruction(ilist, I_JUMP, synonym, NULL, labelEndWhile)) return E_INTERNAL;  //create instruction for jump
    if (synonym->dataType != D_BOOL) return E_SEMANTIC_EXP;

    if (ParsToken.tok != DO) return E_SYNTACTIC;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != BEGIN) return E_SYNTACTIC;
    result = CompoundStatement(false, ilist);

    if (InsertInstruction(ilist, I_JUMP, NULL, NULL, labelExpr)) return E_INTERNAL;  //create instruction for jump

    if (InsertInstruction(ilist, I_LAB, NULL, NULL, NULL)) return E_INTERNAL;  //create label
    labelEndWhile->value.label = ilist->last;  //save pointer to instruction in body of while

    return result;
}

/**
 * @desc checking syntax of non-terminal <If>
 * @param *ilist - list of instruction
 * @return errorValue
 */
int If (tInstList *ilist){  //<If> -> if <Expression> then <CompoundStatement> else <CompoundStatement>
    int result;

    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    if (GetToken(&s)) return E_LEXICAL;
    TSynonym *synonym;  //save result of Expression()
    if ((result = Expression(hash, &synonym, ilist)) != E_OK) return result;
    if (synonym->dataType != D_BOOL) return E_SEMANTIC_EXP;
    
    TSynonym *labelElse, *labelEndIf;
    TDataValue valueElse, valueEndIf;
    valueElse.label = NULL;
    valueEndIf.label = NULL;
    if ((labelElse = NewSynonym(hash, NULL, I_LABEL, D_LABEL, valueElse, false)) == NULL) return E_INTERNAL;
    if ((labelEndIf = NewSynonym(hash, NULL, I_LABEL, D_LABEL, valueEndIf, false)) == NULL) return E_INTERNAL;

    if (InsertInstruction(ilist, I_NOT, synonym, NULL, NULL)) return E_INTERNAL; //negate condition
    if (InsertInstruction(ilist, I_JUMP, synonym, NULL, labelElse)) return E_INTERNAL;  //jump to else

    if (ParsToken.tok != THEN) return E_SYNTACTIC;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != BEGIN) return E_SYNTACTIC;
    if ((result = CompoundStatement(false, ilist)) != E_OK) return result;

    if (InsertInstruction(ilist, I_JUMP, NULL, NULL, labelEndIf)) return E_INTERNAL;  //jump to end of if-statement
    if (InsertInstruction(ilist, I_LAB, NULL, NULL, NULL)) return E_INTERNAL;  //insert label
    labelElse->value.label = ilist->last;  //save pointer to instruction in body of else

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ELSE) return E_SYNTACTIC;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != BEGIN) return E_SYNTACTIC;
    result = CompoundStatement(false, ilist);

    if (InsertInstruction(ilist, I_LAB, NULL, NULL, NULL)) return E_INTERNAL;  //insert label
    labelEndIf->value.label = ilist->last;  //save pointer to instruction in body of else

    return result;
}

/**
 * @desc checking syntax of non-terminal <Function>
 * @param *ilist - list of instruction
 * @return errorValue
 */
int Function (tInstList *ilist, TSynonym *idFunction, TSynonym *variable){  //<Function> -> ID ( <FirstTerm> <Term> )

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != LBR) return E_SYNTACTIC;

    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    TSynonym *tmp;
    TSynonym *param;
    TDataValue value;
    string funcParam;
    if (strInit(&funcParam)) return E_INTERNAL;

    TSynonym *newHash = malloc(sizeof(struct TSynonym));
    InsertInstruction(ilist, I_FRAME, idFunction, NULL, newHash);  //copy TS

    //strAddChar(&funcParam, idFunction.value.string->str[0]);
    int i = 0;
    do{
	i++;
	if (GetToken(&s)) return E_LEXICAL;
	switch (ParsToken.tok){
	    case DOUBLE:
		if (idFunction->value.function.funcParams[i] != 'r') return E_SEMANTIC_EXP;  //check type of parameter of index i
		value.real = ParsToken.dnum;
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_REAL, value, true)) == NULL) return E_INTERNAL;
		tmp->inicialized = true;
		break;
	    case INT:
		if (idFunction->value.function.funcParams[i] != 'i') return E_SEMANTIC_EXP;  //check type of parameter of index i
		value.integer = ParsToken.inum;
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_INT, value, true)) == NULL) return E_INTERNAL;
		tmp->inicialized = true;
		break;
	    case STR:
		if (idFunction->value.function.funcParams[i] != 's') return E_SEMANTIC_EXP;  //check type of parameter of index i
		value.string = malloc(sizeof (string));
		strInit(value.string);
		strCopyString(value.string, &ParsToken.key);
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_STRING, value, true)) == NULL) return E_INTERNAL;
		tmp->inicialized = true;
		break;
	    case TTRUE:
		if (idFunction->value.function.funcParams[i] != 'b') return E_SEMANTIC_EXP;  //check type of parameter of index i
		value.boolean = true;
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
		tmp->inicialized = true;
		break;
	    case FFALSE:
		if (idFunction->value.function.funcParams[i] != 'b') return E_SEMANTIC_EXP;  //check type of parameter of index i
		value.boolean = false;
		if ((tmp = NewSynonym(hash, NULL, I_CONST, D_BOOL, value, true)) == NULL) return E_INTERNAL;
		tmp->inicialized = true;
		strAddChar(&funcParam, 'b');
		break;
	    case ID:
		if (((tmp = FindVariable(ParsToken.key.str)) == NULL) || (tmp->type != I_VAR)) return E_SEMANTIC_EXP;
		if (tmp->dataType == D_INT) { if (idFunction->value.function.funcParams[i] != 'i') return E_SEMANTIC_EXP; }  //check type of parameter of index i
		else if (tmp->dataType == D_REAL) { if (idFunction->value.function.funcParams[i] != 'r') return E_SEMANTIC_EXP; }
		else if (tmp->dataType == D_STRING) { if (idFunction->value.function.funcParams[i] != 's') return E_SEMANTIC_EXP; }
		else if (tmp->dataType == D_BOOL) { if (idFunction->value.function.funcParams[i] != 'b') return E_SEMANTIC_EXP; }
	    default:
		return E_SYNTACTIC;
		break;
	}
	param = malloc(sizeof(struct TSynonym));
	param->paramCounter = i;
	InsertInstruction(ilist, I_SET, newHash, tmp, param);

	if (GetToken(&s)) return E_LEXICAL;
    } while (ParsToken.tok == COMMA);

    if (ParsToken.tok != RBR) return E_SYNTACTIC;

   /* value.label = NULL;
    TSynonym *label = NULL;
    if ((label = NewSynonym(idFunction->value.function.localTable, NULL, I_LABEL, D_LABEL, value, false)) == NULL) return E_INTERNAL;
    value.label = idFunction->value.function.instList->first;*/


    TSynonym *newIlist = malloc(sizeof(struct TSynonym));
    newIlist->value.function.instList = malloc(sizeof(struct tInstList));
    InsertInstruction(ilist, I_CIL, idFunction, NULL, newIlist);  //make copy of instruction list

    InsertInstruction(ilist, I_ILS, newIlist, newHash, NULL);  //change pointers in instruction list

    InsertInstruction(ilist, I_REC, newIlist, NULL, NULL);  //change pointers in instruction list
  /*  TSynonym *labelGlobal = NULL;
    TSynonym *jumpFirst = malloc(sizeof(struct TSynonym));
    jumpFirst->inicialized = true;
    InsertInstruction(ilist, I_JUMP, NULL, NULL, labelGlobal);  //jump to local instruction list (instruction in global IL)*/

   /* TDataValue valueGlobal;
    if ((labelGlobal = NewSynonym(sTable, NULL, I_LABEL, D_LABEL, valueGlobal, false)) == NULL) return E_INTERNAL;*/

    InsertInstruction(ilist, I_RET, newHash, NULL, variable);  //return value from function
//    valueGlobal.label = ilist->active;

    return E_OK;
}

/**
 * @desc checking syntax of non-terminal <Assignment>
 * @param *variable - variable where save result of expression or function
 * @param *ilist - list of instruction
 * @return errorValue
 */
int Assignment (TSynonym *variable, tInstList *ilist){  //<Assignment> -> ID := <Assign>
    int result;

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ASSIGN) return E_SYNTACTIC;

    if (GetToken(&s)) return E_LEXICAL;	

    TSynonym *synonym;
    TSynonym *tmp;
    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS
    switch (ParsToken.tok){
	case SORT:
	case FIND:
	    result = BuiltIn(ilist, variable);
	    if (GetToken(&s)) return E_LEXICAL;
	    break;
	case ID:
	    if (strcmp(ParsToken.key.str, "copy") == 0) {  //built-in function copy
		result = BuiltIn(ilist, variable); 
		if (GetToken(&s)) return E_LEXICAL; 
		break; 
	    } 
	    else if (strcmp(ParsToken.key.str, "length") == 0) {  //built-in function length
		result = BuiltIn(ilist, variable); 
		if (GetToken(&s)) return E_LEXICAL; 
		break; 
	    }  

	    //if variable or function doesn't exist its semantic error
	    if ((tmp = FindVariable(ParsToken.key.str)) == NULL) return E_SEMANTIC_VAR;

	    if (tmp->type == I_FUNC) { 
		result = Function(ilist, tmp, variable);
		if (GetToken(&s)) return E_LEXICAL;
	    }
	    else {
		if ((result = Expression(hash, &synonym, ilist)) != E_OK) return result;
		if (synonym->dataType != variable->dataType) return E_SEMANTIC_EXP;
	    	if (InsertInstruction(ilist, I_MOV, synonym, NULL, variable)) return E_INTERNAL;
	    }
	    break;
	case DOUBLE:
	case INT:
	case STR:
	case FFALSE:
	case LBR:
	case TTRUE:
	    if ((result = Expression(hash, &synonym, ilist)) != E_OK) return result;
	    if (synonym->dataType != variable->dataType) return E_SEMANTIC_EXP;

	    if (InsertInstruction(ilist, I_MOV, synonym, NULL, variable)) return E_INTERNAL;
	    break;
	default:
	    return E_SYNTACTIC;
	    break;
    }

    return result;  
}

/**
 * @desc checking syntax of non-terminal <Statement>
 * @param first - is this function called for first time in a row
 * @param *ilist - list of instruction
 * @return errorValue
 */
int Statement (bool first, tInstList *ilist, bool assign){
    int result;

    if (first == false){  //this isn't first statement in a row
	if (!assign){ if (GetToken(&s)) return E_LEXICAL; }
	assign = false;
	if (ParsToken.tok != SEMICOL) return E_OK;  //<Statement> -> Eps
        if (GetToken(&s)) return E_LEXICAL;	
    }

    TSynonym *variable;
    switch (ParsToken.tok){
	case ID: 
	    if ((strcmp(ParsToken.key.str, "length") == 0) || ((strcmp(ParsToken.key.str, "copy") == 0))) return E_SYNTACTIC;
	    if ((variable = FindVariable(ParsToken.key.str)) == NULL) return E_SEMANTIC_VAR;

	    if (variable->type == I_VAR) { 
		if ((result = Assignment(variable, ilist)) != E_OK) return result;  //<Statement> -> <Assignment>
		assign = true;
	    }
	    else return E_SYNTACTIC;
	    break;
	case IF:
	    if ((result = If(ilist)) != E_OK) return result;  //<Statement> -> <If>
	    break;
	case WHILE:
	    if ((result = While(ilist)) != E_OK) return result;  //<Statement> -> <While>
	    break;
	case WRITE:
	    if ((result = Write(ilist)) != E_OK) return result;  //<Statement> -> <Write>
	    break;
	case READLN:
	    if ((result = Readln(ilist)) != E_OK) return result;  //<Statement> -> <Readln>
	    break;
	default: 
	    if (first == true)	return E_OK;  //<CompoundStatement> -> Eps
	    else return E_SYNTACTIC;
	    break;
    }

    result = Statement(false, ilist, assign);
    return result;
}

/**
 * @desc checking syntax of non-terminal <CompoundStatement>
 * @param function - is this function called from FunctionDefinition
 * @param *ilist - list of instruction
 * @return errorValue
 */
int CompoundStatement (bool mainBody, tInstList *ilist){  //<CompoundStatement> -> begin <FirstCompStatement> <CompStatement> end
    int result;

    if (GetToken(&s)) return E_LEXICAL;
    if ((result = Statement(true, ilist, false)) != E_OK) return result;

    if (mainBody != true){
	if (ParsToken.tok != END) return E_SYNTACTIC;
    }

    return E_OK;
}

/**
 * @desc checking syntax of non-terminal <MainBody>
 * @return errorValue
 */
int MainBody(){
    int result;

    if (ParsToken.tok != BEGIN) return E_SYNTACTIC;  //<MainBody> -> begin <FirstCompStatement> <CompStatement> end.
    if ((result = CompoundStatement(true, &ilist)) != E_OK) return result;
    if (ParsToken.tok != ENDDOT) return E_SYNTACTIC;
    InsertInstruction(&ilist, I_STOP, NULL, NULL, NULL);
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ENDFILE) return E_SYNTACTIC;

    return E_OK;
}

/**
 * @desc checking syntax of non-terminal <FunctionParameter>
 * @param first - is this function called for first time in a row
 * @param *funcparam - string where letters for type of variables are saved
 * @param *hash - pointer to global or local TS
 * @param insert - we're gonna to save variables or not
 * @param counter - counter for parameters
 * @return errorValue
 */
int FunctionParameter(bool first, string *funcParam, TSymTable *hash, bool insert, int counter){
    int result;

    if (first == false){ //<FunctionParameter> -> ; ID : <Type> <FunctionParameter>
	if (GetToken(&s)) return E_LEXICAL;
	if (ParsToken.tok != SEMICOL) return E_OK;  //<FunctionParameter> -> Eps
    }

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ID) return E_OK;  //<FunctionParameter> -> Eps

    TSynonym *tmp = SearchTable(hash, ParsToken.key.str);
    //it's first declaration/definition of function so name of variable mustn't be in TS
    if ((insert == true) && (tmp != NULL)) return E_SEMANTIC_VAR;
    //it's definition of already declared function so name of variable have to be in TS
    else if ((insert == false) && (tmp == NULL)) return E_SEMANTIC_VAR;
    strCopyString(&key, &ParsToken.key);  //must remember ID of the variable

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != COLON) return E_SYNTACTIC;

    TDataValue value;
    if (GetToken(&s)) return E_LEXICAL;
    switch (ParsToken.tok){
	case INTEGER:
	    if (insert == true){  //if this function was already declared we don't need to insert variable into TS
		value.integer = 0;
		if ((tmp = NewSynonym(hash, key.str, I_VAR, D_INT, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = counter;
	    }
	    strAddChar(funcParam, 'i'); break;
	case REAL:
	    if (insert == true){
		value.real = 0.0;
		if ((tmp = NewSynonym(hash, key.str, I_VAR, D_INT, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = counter;
	    }
	    strAddChar(funcParam, 'r'); break;
	case STRING:
	    if (insert == true){
    		value.string = malloc(sizeof (string));
    		strInit(value.string);
		if ((tmp = NewSynonym(hash, key.str, I_VAR, D_INT, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = counter;
	    }
	    strAddChar(funcParam, 's'); break;
	case BOOLEAN:
	    if (insert == true){
		value.boolean = false;
		if ((tmp = NewSynonym(hash, key.str, I_VAR, D_INT, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = counter;
	    }
	    strAddChar(funcParam, 'b'); break;
	default:
	    return E_SYNTACTIC;
    }

    result = FunctionParameter(false, funcParam, hash, insert, counter++);  //recusrive call of function
    return result;
}

/**
 * @desc checking syntax of non-terminal <FunctionDefinition>
 * @return errorValue
 */
int FunctionDefinition(){
    int result;

    //<FunctionDefinition> -> function ID ( <FirstFunctionParameter> <FunctionParameter> ) : <type> ; <CompoundStatement> ; <FunctionDefinition>
    if (ParsToken.tok != FUNCTION) return E_OK;  //<FunctionDefinition> -> Eps
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != ID) return E_SYNTACTIC; //next token must be ID

    TSynonym *tmp;
    string funcKey;
    strInit(&funcKey);
    bool insert = true;
    //if function or variable with this ID already exist or function is already defined it's semantic error
    if (((tmp = SearchTable(sTable, ParsToken.key.str)) != NULL) && (tmp->inicialized == true)) return E_SEMANTIC_VAR;
    //function was declared but wasn't defined. So there's no need to save variables, just check head of function
    else if ((tmp != NULL) && (tmp->inicialized == false)) insert = false;
    strCopyString(&funcKey, &ParsToken.key);  //must remember ID of the function

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != LBR) return E_SYNTACTIC;  //"("

    string funcParam, funcString;  //variables to save types of functon parameters
    if (strInit(&funcString)) return E_INTERNAL;
    if (strInit(&funcParam)) return E_INTERNAL;

    TSymTable *hash;
    if (tmp == NULL) hash = InitTab(1303);
    else hash = tmp->value.function.localTable;

    //function for non-terminals <FirstFunctionParameter> & <FunctionParameter>
    if ((result = FunctionParameter(true, &funcParam, hash, insert, 1)) != E_OK) return result;

    if (ParsToken.tok != RBR) return E_SYNTACTIC;  //")"

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != COLON) return E_SYNTACTIC;  //":"
    if (GetToken(&s)) return E_LEXICAL;

    TDataValue value;
    //add char to string on first position and insert new variable into local hash table (same name and type as function)
    switch (ParsToken.tok){
	case INTEGER:
	    if (tmp == NULL){
		value.integer = 0;
		if ((tmp = NewSynonym(hash, funcKey.str, I_VAR, D_INT, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = -1;
	    }
	    strAddChar(&funcString, 'i'); break;
	case REAL:
	    if (tmp == NULL){
		value.real = 0.0;
		if ((tmp = NewSynonym(hash, funcKey.str, I_VAR, D_REAL, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = -1;
	    }
	    strAddChar(&funcString, 'r'); break;
	case STRING:
	    if (tmp == NULL){
    		strInit(value.string);
		if ((tmp = NewSynonym(hash, funcKey.str, I_VAR, D_STRING, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = -1;
		strClear(value.string);
	    }
	    strAddChar(&funcString, 's'); break;
	case BOOLEAN:
	    if (tmp == NULL){
		value.boolean = false;
		if ((tmp = NewSynonym(hash, funcKey.str, I_VAR, D_BOOL, value, false)) == NULL) return E_INTERNAL;
		tmp->paramCounter = -1;
	    }
	    strAddChar(&funcString, 'b'); break;
	default:
	    return E_SYNTACTIC;
    }
    strAddStr(&funcString, &funcParam);  //making final form of informational string of function

    //insert function to TS
    if ((insert == true) && ((tmp = NewSynonym(sTable, funcKey.str, I_FUNC, D_FUNC, value, false)) == NULL)) //insert to TS failed
	return E_INTERNAL;
    else if (insert == true) {
	tmp->value.function.funcParams = malloc(sizeof(char)*strlen(funcString.str));
	strcpy(tmp->value.function.funcParams, funcString.str);
	tmp->value.function.localTable = hash;
	tmp->value.function.instList = malloc(sizeof (struct tInstList));
	InstListInit(tmp->value.function.instList);
    }
    //checking if declration and definiton of function is same
    if ((insert == false) && (strcmp(funcString.str, tmp->value.function.funcParams) != 0))
	return E_SEMANTIC_VAR;

    strFree(&funcParam);
    strFree(&funcString);

    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != SEMICOL) return E_SYNTACTIC;  //";"

    if (GetToken(&s)) return E_LEXICAL;

    TStackElement stackElement;
    stackElement.next = NULL;
    stackElement.table = hash;
    PushStack(stack, stackElement);  //local TS is add on stack

    switch (ParsToken.tok){
	case VAR:
	    if ((result = GlobalVariable(true)) != E_OK) return result;  //local definition of variables
	case BEGIN:
	    if ((result = CompoundStatement(false, tmp->value.function.instList)) != E_OK) return result;  //body of the function
	    tmp->inicialized = true;
    InsertInstruction(tmp->value.function.instList, I_STOP, NULL, NULL, NULL);  //jump to global IL (istruction in local TS)
	    break;
	case FORWARD:  //forward declaration
	    break;
	default:  //bad syntax
	    return E_SYNTACTIC;
    }
    //tmp->value.function.instList = funcIList;
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != SEMICOL) return E_SYNTACTIC;  //";"
    if (GetToken(&s)) return E_LEXICAL;

    PopStack(stack);  //local TS is add on stack

    result = FunctionDefinition();  //recursive call of function
    return result;
}

/**
 * @desc checking syntax of non-terminal <GlobalVariable>
 * @hash - pointer to global or local TS
 * @param first - is this function called for first time in a row
 * @return errorValue
 */
int GlobalVariable(bool first){
    int result;
    TSymTable *hash;
    if (EmptyStack(stack)) hash = sTable;  //empty stack -> global TS
    else hash = TopStack(stack)->table;  //else -> local TS

    if (GetToken(&s)) return E_LEXICAL;
    if (first == false){  //not a first global variable in a row
	if (ParsToken.tok != ID) return E_OK; //<GlobalVariable> -> Eps
    }
    else  //first global variable in a row
	if (ParsToken.tok != ID) return E_SYNTACTIC; //<first global variable can't be Eps

    //if variable with this ID already exist it's semantic error
    if (SearchTable(hash, ParsToken.key.str) != NULL) return E_SEMANTIC_VAR;

    if (strcmp(ParsToken.key.str, "length") == 0) return E_SEMANTIC_VAR;  //redefine built-inf function
    else if (strcmp(ParsToken.key.str, "copy") == 0) return E_SEMANTIC_VAR;  //redefine built-inf function

    strCopyString(&key, &ParsToken.key);  //must remember ID of the variable

    //<GlobalVariable> -> ID : <Type> ; <GlobalVariable>
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != COLON) return E_SYNTACTIC;  //next token must be COLON

    if (GetToken(&s)) return E_LEXICAL;
    TDataValue value;
    switch (ParsToken.tok){  //checking non-terminal <Type>
	case INTEGER:  //insert variable into hash table
	    value.integer = 0;
	    if (NewSynonym(hash, key.str, I_VAR, D_INT, value, false) == NULL) return E_INTERNAL;
	    break;
	case REAL:
	    value.real = 0.0;
	    if (NewSynonym(hash, key.str, I_VAR, D_REAL, value, false) == NULL) return E_INTERNAL;
	    break;
	case STRING:
	    value.string = malloc(sizeof (string));
	    if (NewSynonym(hash, key.str, I_VAR, D_STRING, value, false) == NULL) return E_INTERNAL;
	    break;
	case BOOLEAN:
	    value.boolean = false;
	    if (NewSynonym(hash, key.str, I_VAR, D_BOOL, value, false) == NULL) return E_INTERNAL;
	    break;
	default: return E_SYNTACTIC;  //if it is not one of allowed type it's error
    }
		
    if (GetToken(&s)) return E_LEXICAL;
    if (ParsToken.tok != SEMICOL) return E_SYNTACTIC;  //next token must be SEMICOL

    result = GlobalVariable(false);  //recursive call of function
    return result;
}

/**
 * @desc Checks if all declared functions are defined
 * @return errorValue
 */
int AllFunctionDefined(){
    TSynonym *tmp;

    for (int i = 0; i < sTable->sizeOfTable; i++){
	tmp = sTable->columns[i];
	while (tmp != NULL){
	    if ((tmp->type == I_FUNC) && (tmp->inicialized == false)) return E_SEMANTIC_VAR;
	    tmp = tmp->nextSynonym;
	}
    }
    return E_OK;
}

/**
 * @desc Starts parsing file
 * @return errorValue
 */
int Parse(){
    int result;

    stack = malloc(sizeof (struct TStack));
    InitStack(stack);

    if (strInit(&key)) { ErrorAdd(&error, LINE_CNT, E_INTERNAL); return ERR_FLAG; }  //initialization of global variable
    if (strInit(&s)) { ErrorAdd(&error, LINE_CNT, E_INTERNAL); return ERR_FLAG; }

    if (GetToken(&s)) { ErrorAdd(&error, LINE_CNT, E_LEXICAL); return ERR_FLAG; } //Get first token

    switch (ParsToken.tok){
	case VAR:  //<GlobalDefinition> -> VAR <FirstGlobalVariable>
	    if ((result = GlobalVariable(true)) != E_OK) { ErrorAdd(&error, LINE_CNT, result); return ERR_FLAG; }
	case FUNCTION:  //<GlobalDefiniton> -> Eps or just end of global definition
	    if ((result = FunctionDefinition()) != E_OK) { ErrorAdd(&error, LINE_CNT, result); return ERR_FLAG; }
	default:  //<FunctionDefinition> -> Eps or just end of function definition
	    if ((result = AllFunctionDefined()) != E_OK) { ErrorAdd(&error, LINE_CNT, result); return ERR_FLAG; }
	    if ((result = MainBody()) != E_OK) { ErrorAdd(&error, LINE_CNT, result); return result; }
    }
    DeleteStack(stack);
    strFree(&s);
    ErrorAdd(&error, LINE_CNT, result); return ERR_FLAG;
}
