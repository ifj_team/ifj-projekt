/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#include "instlist.h"

extern int LINE_CNT;		/* LINE COUNTER */
extern int ERR_FLAG;		/* ERROR FLAG */
extern tErrorHandler error;	/* ERROR HANDLER */

/**
 * @decs Initialization, instructions list
 * @param *L Instructions list
 */
void InstListInit( tInstList *L ) {
  L->first = NULL;
  L->last = NULL;
  L->active = NULL;
}

/**
 * @desc Free allocated memory of instructions list
 * @param *L Instructions List
 */
void InstListDispose( tInstList *L ) {
  tInstPtr DisposeI;

  while( L->first != NULL ){   
    DisposeI = L->first;
    L->first = L->first->nextItem;
    free(DisposeI);
  }
  InstListInit(L);
}

int InsertInstruction( tInstList *L, iInstruction type, TSynonym *src1, TSynonym *src2, TSynonym *dest ) {
  
    tInstPtr InsertInst;
    
    if ( (InsertInst = (tInstPtr)malloc(sizeof (struct tInst))) != NULL ) {
        InsertInst->type = type;
        InsertInst->src1 = src1;
        InsertInst->src2 = src2;
        InsertInst->dest = dest;
        InsertInst->nextItem = NULL;
	
	if( L->first == NULL ) {
	  L->first = InsertInst;
	}
	else {
	  L->last->nextItem = InsertInst;
	}
	L->last = InsertInst;
	
        return ERR_FLAG;
    }
    else {
      return E_INTERNAL;
    }
}

/**
 * @desc test activity
 */
bool InstListActive( tInstList *L ) {
  return( L->active != NULL)?true:false;
}

/**
 * @desc Set first item as active item
 * @param *L Instructions list
 */
void InstListSetFirst( tInstList *L ) {
  L->active = L->first;
}

/**
 * @desc shift activity to the next item
 * @param *L Instructions list
 */
void InstListSucc( tInstList *L ) {
  if( L->active != NULL ) {
    if( L->active->nextItem != NULL ) {
      L->active = L->active->nextItem;
    }
    else L->active = NULL;
  }
  else L->active = NULL;
}
/**
 * @desc copy Instruction List
 * @param *L Instruction list
 * @return pointer to copy of instruction list
 */
tInstList* InstListCopy( tInstList *L ){
    tInstList *newL = malloc(sizeof(struct tInstList));
    tInstPtr I;
    InstListInit(newL);
    /* instruction list is empty */
    if( !L->first ) return NULL;

    /* set first item as active */
    InstListSetFirst(L);

    while( InstListActive(L) ){
	
	if( (I = malloc(sizeof(struct tInst))) != NULL ) {
	    I->type = L->active->type;
	    I->src1 = L->active->src1;
	    I->src2 = L->active->src2;
	    I->dest = L->active->dest;
	    I->nextItem = NULL;
	    
	    if( newL->first == NULL ) {
		newL->first = I;
	    }
	    else {
		newL->last->nextItem = I;
	    }
	newL->last = I;
	}
	else return NULL;
	/* shift activity */
	InstListSucc(L);
    }
    return newL;
}