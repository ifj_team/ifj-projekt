/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01   <<<<<<                              *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/

#ifndef STR_H
#define STR_H

#include <string.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include "errorhandler.h"

/**
 * STR_LEN_INC define on what amount of bytes we make the first allocation of memory
 * if we're reading it char by char, memory will be allocated gradually on multiple by this number
 */
#define STR_LEN_INC 8

typedef struct {
  char* str;		// place for current string which ends with char '\0'
  int length;		// strings length
  int allocSize;	// size of allocated memory
} string;

/**
 * Functions of str.c
 * @function strClear - remove content of string
 * @function strAddChar - add char at the end of string
 * @function strCopyString - copy string from s2 to s1 
 * @function strAddStr - copy s2 to the end of s1
 * @function Copy - take out the substring from string
 * @function strCmpString - compare two strings
 * @function strCmpConstStr - compare our string with const string
 * @function strGetStr - return text part of string
 * @function strGetLength - return length of string
 * @function numInit - allocation of memory for int and double number
 * @function numFree - free allocated memory for number values
 * @function numStrToI -converts string into integer and check if this number don't overflow INT_MAX limit
 * @function numStrToD - converts string into double and chechk if this number don't overflow
 */
int numStrToI(string *s, int *inum);
int numStrToD(string *s, double *dnum);
int strInit(string *s);
void strFree(string *s);
bool strClear(string *s);
int strAddChar(string *s1, char c);
int strCopyString(string *s1, string *s2);
int strAddStr(string *s1, string *s2);
string* Copy (string *s, int ind, int len);
int strCmpString(string *s1, string *s2);
int strCmpConstStr(string *s1, char *s2);
char *strGetStr(string *s);
int strGetLength(string *s);

#endif