/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01   <<<<<<                              *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/

#include "str.h"

extern tErrorHandler error;	/* Error Handler */
extern int LINE_CNT;		/* LINE COUNTER */
extern int ERR_FLAG;		/* ERROR FLAG */

/**
 * @desc - converts string into integer and check if this number don't overflow INT_MAX limit
 */
int numStrToI(string *s, int *inum) {
   *inum = 0;
   //int num;
   for (int i = 0; s->str[i] != '\0'; i++) {
      // check if inum don't overflow
      if ((*inum * 10 + (s->str[i] - '0')) <= INT_MAX && (*inum * 10 + (s->str[i] - '0')) >= 0)
         *inum = *inum * 10 + (s->str[i] - '0');
      else {
         return E_INTERNAL;
      }
   }
   //*inum = num;
   /* WITHOUT ERROR */
   return E_OK;
}

/**
 * @desc - Firstly exchange character '+' or '-' with 'e' or 'E', so we can use
 *         function strtod to convert string into double. Function strtod converts
 *         only numbers like that: 12e+2 but in IFJ14 looks the exponent like this:
 *         12+e2. So we need to exchange 'e' and '+'.
 *         Next task is to convert string to double using func strtod.
 */
int numStrToD(string *s, double *dnum) {
   *dnum = 0;
   char *pEnd;

   *dnum = strtod(s->str, &pEnd);
   // Check if dnum don't overflow
   if (errno == ERANGE) {
      return E_INTERNAL;
   }
   else
      /* WITHOUT ERROR */
      return E_OK;
}

/**
 * @desc - allocation of memory for new string
 * @param string *s - structure with string
 */
int strInit(string *s) {
   if( (s->str = malloc(STR_LEN_INC)) == NULL ) {
      return E_INTERNAL;
   }
   s->str[0] = '\0';
   s->length = 0;
   s->allocSize = STR_LEN_INC;
   /* WITHOUT ERROR */
   return E_OK;
}

/**
 * @desc - free allocated memory of string
 * @param string *s - structure with string
 */
void strFree(string *s) {
   free(s->str);
}

/**
 * @desc - remove content of string
 * @param string *s - string
 */
bool strClear(string *s) {
   if (s) {
        free(s->str);
        s->str = (char *)malloc(STR_LEN_INC);
        if (s->str) {
	    s->str[0] = '\0';
	    s->length = 0;
	    s->allocSize = STR_LEN_INC;
	    return true;
	}
    }
    return false;
}

/**
 * @desc - add char at the end of string
 * @param string *s1 - string
 *        char c - char for adding
 */
int strAddChar(string *s1, char c) {
   if (s1->length + 1 >= s1->allocSize) {
      // we need more memory, it's time for reallocation
      if ((s1->str = (char*) realloc(s1->str, s1->length + STR_LEN_INC)) == NULL) {
         return E_INTERNAL;
      }
      s1->allocSize = s1->length + STR_LEN_INC;
   }
   s1->str[s1->length] = c;
   s1->length++;
   s1->str[s1->length] = '\0';
   /* WITHOUT ERROR */
   return E_OK;
}

/**
 * @desc - copy string from s2 to s1
 * @param string *s1, *s2 - retezce
 */
int strCopyString(string *s1, string *s2) {
   int newLength = s2->length;
   if (newLength >= s1->allocSize) {
      // we need more memory, it's time for reallocation
      if ((s1->str = (char*) realloc(s1->str, newLength + 1)) == NULL) {
         return E_INTERNAL;
      }
      s1->allocSize = newLength + 1;
   }
   strcpy(s1->str, s2->str);
   s1->length = newLength;
   /* WITHOUT ERROR */
   return E_OK;
}


/**
 * @desc - copy string s2 to the end of s1, for example:
 *         s1->str is "byebye", s2->str is "firsttime" and the result in
 *         s1->str is "byebyefirsttime"
 */
int strAddStr(string *s1, string *s2) {

   int newLength = s1->length + s2->length;

   // need more memory
   if (newLength >= s1->allocSize) {
      if ((s1->str = (char*) realloc(s1->str, newLength + 1)) == NULL) {
         return E_INTERNAL;
      }
      s1->allocSize = newLength + 1;
   }

   // lets connect these two strings
   for (int i = s1->length; i < newLength; i++) {
      s1->str[i] = s2->str[s2->length - newLength + i];
   }
   s1->str[newLength] = '\0';
   s1->length = newLength;
   /* WITHOUT ERROR */
   return E_OK;
}

/*
 * @desc - return substring which starts on index ind and has length len. Substring replace original string.
 * @param string *s - original string
 *        int ind - starting index (ind >= 1 -> pasacl indexation)
 *        int len - length of the substring
 */
string* Copy (string *s, int ind, int len) {
   int i; // for cycle

   // if len is larger than length of string s, the result is truncated
   if ((ind + len - 1) > s->length)
      len = s->length - ind + 1;
   // if ind is larger than the length of string s, empty string is returned
   if (ind > s->length || len == 0) {
      s->str[0] = '\0';
      s->length = 0;
      return s;
   }
   if (len != s->length) {
      // lengths are different - whole original string won't be substring
      if (ind > 0) {
         // converting to C indexation - starting index is 0
         ind = ind - 1;
         char *tmp; // temporary string
         if ((tmp = (char *) malloc(sizeof(char) * (len + 1))) == NULL) {
            return NULL;
         }

         // now we copy the substring to the temporary string
         for (i = ind; i <= (ind + len - 1); i++)
            tmp[i - ind] = s->str[i];
         tmp[len] = '\0';
         // and copy temporary string into s->str
         strcpy(s->str, tmp);
         s->length = len;
         s->str[s->length] = '\0';
         free(tmp);
      }
      else {
         return NULL;
      }
   }
   /* WITHOUT ERROR */
   return s;
}

/**
 * @desc - compare two strings
 * @param string *s1, *s2 - strings for compare
 */
int strCmpString(string *s1, string *s2) {
   return strcmp(s1->str, s2->str);
}

/**
 * @desc - compare our string with const string
 * @param string *s1 - structure with our string
 *        char* s2 - const string
 */
int strCmpConstStr(string *s1, char* s2) {
   return strcmp(s1->str, s2);
}

/**
 * @desc - return text part of string
 * @param string *s - structure of string
 */
char *strGetStr(string *s) {
   return s->str;
}

/**
 * @desc - return length of string
 * @param string *s - string
 */
int strGetLength(string *s) {
   return s->length;
}
