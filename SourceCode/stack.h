/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k   <<<<<<                              *
 *                                                                     *
 ***********************************************************************/


#ifndef STACK
#define STACK

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ial.h"

//Structure of one stack element
typedef struct TStackElement {
    struct TStackElement *next;
    TSymTable *table;
} TStackElement;

//Structure of stack
typedef struct TStack {
    TStackElement *top;
} TStack;

/**
 * @brief Inicializaton of stack
 * @param *stack Pointer to stack
 */
void InitStack(TStack *stack);

/**
 * @brief Push one element to the stack
 * @param *stack Pointer to stack
 * @param Element which should be put into the stack
 * @return True if everything is OK, false if not
 */
bool PushStack(TStack *stack, TStackElement element);

/**
 * @brief Pop one element from the stack
 * @param *stack Pointer to stack
 * @return True if everything is OK, false if not
 */
bool PopStack(TStack *stack);

/**
 * @brief Read one element from top of the stack
 * @param *stack Pointer to stack
 * @return Pointer to element on top of the stack
 */
TStackElement* TopStack(TStack *stack);

/**
 * @brief Finds out, if the stack is empty or not
 * @param *stack Pointer to stack
 * @return Tru if empty, false if not   
 */
bool EmptyStack(TStack *stack);

/**
 * @brief Deletes the stack
 * @param Pointer to stack
 */
void DeleteStack(TStack *stack);

#endif
