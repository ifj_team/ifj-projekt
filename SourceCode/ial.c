/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k   <<<<<<                              *
 *                                                                     *
 ***********************************************************************/


#include "ial.h"

extern int ERR_FLAG;		/* ERROR FLAG */
extern int LINE_CNT;		/* LINE COUNTER */
extern tErrorHandler error;	/* ERROR HANDLER */

/**
 * @brief Function to generate hash key
 * @param *key Name of function, variable or constant 
 * @param sizeOfTable Size of table of symbols
 * @return 
 */
unsigned int GetHash(char *key, int sizeOfTable) {
    unsigned int hash = 0;
    int  length;

    length = strlen(key);    

    for(int i = 0; i < length; i++) { 
        hash = 29 * hash + key[i];
    }
    
    return (hash % sizeOfTable);
}

/**
 * @brief Inicialization of table of symbols
 * @param sizeOfTable Size of table of symbols
 * @return Pointer to new table
 */
TSymTable* InitTab(int sizeOfNewTable) {
    TSymTable *newTable;

    //Allocating memory for table
    newTable = malloc(sizeof(TSymTable));
    if(newTable == NULL) {
	    return NULL;
    }

    //Allocating memory for columns of the table
    newTable->columns = malloc(sizeOfNewTable * sizeof(TSynonym));
    if(newTable->columns == NULL) {
        free(newTable);
        return NULL;
    }
    else {
	    //All columns points to NULL
        for(int i = 0; i < sizeOfNewTable; i++) {
            newTable->columns[i] = NULL;
        }
    }
    
    newTable->sizeOfTable = sizeOfNewTable;
    newTable->labConNumber = 0;

    return newTable;
}

/**
 * @brief Insert a new element into the table
 * @param *table Table of symbols
 * @param *nameOfToken Name of function, variable, constant, label
 * @param itemType Type of item for saving (function, constant, label, variable)
 * @param dataType Data type of the item for saving (integer, real, boolean, string, label, function)
 * @param dataValue The actual value that is going to be stored
 * @return Pointer to new element
 */
TSynonym* NewSynonym(TSymTable *table, char *nameOfToken, TItemType itemType, TDataType dataType, TDataValue dataValue, bool init) {
    TSynonym *newSynonym;
    int length, key, digitsNumber, i, j = 0;
    char *tmpChar;
   
    //Check if table exists 
    if(table == NULL) {
	    return NULL;
    }

    //Creation of help string for generating constant or label name
    digitsNumber = numOfDigits(INT_MAX);
    tmpChar = malloc((digitsNumber + 2) * sizeof(char)); 
    if(tmpChar == NULL) {
	    return NULL;
    }
    for(int k = 0; k < digitsNumber; k++) tmpChar[k] = '0';
    tmpChar[digitsNumber] = '@';
    tmpChar[digitsNumber + 1] = '\0';

    //Allocation of memory for new item
    newSynonym = malloc(sizeof(struct TSynonym));
    if(newSynonym == NULL) {
	    return NULL;
    }

    //Store name of item and type of item. In case that item is constant or label generate new name
    switch(itemType) {
        case I_VAR:
            length = strlen(nameOfToken);
            newSynonym->name = malloc((length + 1) * sizeof(char));
            strcpy(newSynonym->name, nameOfToken);
            newSynonym->name[length] = '\0';
            newSynonym->type = I_VAR;
            free(tmpChar);
            break;
        case I_FUNC:
            length = strlen(nameOfToken);
            newSynonym->name =  malloc((length + 1) * sizeof(char));
            strcpy(newSynonym->name, nameOfToken);
            newSynonym->name[length] = '\0';
            newSynonym->type = I_FUNC;
            free(tmpChar);
            break;
        case I_LABEL:
            i = table->labConNumber;
            while(i != 0) {
                tmpChar[digitsNumber - j - 1] = (char)(i % 10 + '0');
                j++;
                i = i / 10;
            }
            newSynonym->name = tmpChar;
            newSynonym->type = I_LABEL;
            break;
        case I_CONST:
            i = table->labConNumber;
            while(i != 0) {
                tmpChar[digitsNumber - j - 1] = (char)(i % 10 + '0');
                j++;
                i = i / 10;
            }
            newSynonym->name = tmpChar;
            newSynonym->type = I_CONST;
            break;
        default:
            free(newSynonym);
            return NULL;
    }

    //Store the data type of item and its actual value.
    //In case that item is variable and should not be initalized then init must be false,
    switch(dataType) {
        case D_INT:
            if(!((init != true) && ((itemType == I_VAR) || (itemType == I_CONST)))) newSynonym->value.integer = dataValue.integer;
            newSynonym->dataType = D_INT;
            break;
        case D_REAL:
            if(!((init != true) && ((itemType == I_VAR) || (itemType == I_CONST)))) newSynonym->value.real = dataValue.real;
            newSynonym->dataType = D_REAL;
            break;
        case D_BOOL:
            if(!((init != true) && ((itemType == I_VAR) || (itemType == I_CONST)))) newSynonym->value.boolean = dataValue.boolean;
            newSynonym->dataType = D_BOOL;
            break;
        case D_STRING:
            if(!((init != true) && ((itemType == I_VAR) || (itemType == I_CONST)))) {
                newSynonym->value.string = malloc(sizeof(string));
                if(newSynonym->value.string == NULL) {
                    return NULL;

                }
                strInit(newSynonym->value.string);
                strCopyString(newSynonym->value.string, dataValue.string);
            }
            newSynonym->dataType = D_STRING;
            break;
        case D_LABEL:
            newSynonym->value.label = dataValue.label;
            newSynonym->dataType = D_LABEL;
            break;
        case D_FUNC:
            newSynonym->value.function.funcParams = dataValue.function.funcParams;
            newSynonym->dataType = D_FUNC;
            break;
        default:
            free(newSynonym);
            return NULL;
    } 

    //Generate hash 
    key = GetHash(newSynonym->name, table->sizeOfTable);
 
    //If the new element is not first on the index, then put him in the list
    if(table->columns[key] == NULL) {
        table->columns[key] = newSynonym;
    }
    else {
        TSynonym *tmpSynonym = table->columns[key];

        while (tmpSynonym->nextSynonym != NULL) {
            if(strcmp(newSynonym->name, tmpSynonym->name) == 0) {
                free(newSynonym);
                return NULL;
            }
            tmpSynonym = tmpSynonym->nextSynonym;
        }
        
        tmpSynonym->nextSynonym = newSynonym; 
    }

    if((itemType == I_LABEL) || (itemType == I_CONST)) {
    	table->labConNumber++;
    }

    newSynonym->paramCounter = 0;    
    newSynonym->inicialized = false;

    return newSynonym;
}

/**
 *@brief Count the number of digits in integer
 *@param num Integer for counting
 *@return number of digits
 */
int numOfDigits(int num) {
    int count = 0;

    while(num != 0) {
        num = num / 10;
        count++;
    }
    return count;
}

/**
 *@brief Copy table of Synonyms
 *@param *table Table which should be copied
 *@return Pointer to new table
 */
TSymTable* CopyTable(TSymTable *table) {
    
    TSymTable *newTable;
    TSynonym *newSynonym,  *tmpSynonymOld, *tmpSynonymNew;

    newSynonym = NULL;

     //Allocating memory for table
    newTable = malloc(sizeof(TSymTable));
    if(newTable == NULL) {
        return NULL;
    }

    //Allocating memory for columns of the table
    newTable->columns = malloc(table->sizeOfTable * sizeof(TSynonym));
    if(newTable->columns == NULL) {
        free(newTable);
        return NULL;
    }
    else {
        //All columns points to NULL
        for(int i = 0; i < table->sizeOfTable; i++) {
            newTable->columns[i] = NULL;
        }
    }

    newTable->sizeOfTable = table->sizeOfTable;
    newTable->labConNumber = table->labConNumber;


    for(int j = 0; j < table->sizeOfTable; j++) {
        if(table->columns[j] != NULL) {
            tmpSynonymOld = table->columns[j];
            while(tmpSynonymOld != NULL) {
                newSynonym = CopySynonym(tmpSynonymOld);
                newSynonym->nextSynonym = NULL;                
                if(newTable->columns[j] == NULL) {
                    newTable->columns[j] = newSynonym;
                    tmpSynonymNew = newTable->columns[j];
                    tmpSynonymOld = tmpSynonymOld->nextSynonym;
                }
                else {
                    tmpSynonymNew->nextSynonym = newSynonym;
                    tmpSynonymOld = tmpSynonymOld->nextSynonym;
                    tmpSynonymNew = tmpSynonymNew->nextSynonym;     
                }
            }
        }
    }

    return newTable; 
}

/**
 *@brief Creates a copy of synonym;
 *@param *synonym Synonym which should be copied
 *@return Pointer to new synonym
 */
TSynonym* CopySynonym(TSynonym *synonym) {
    TSynonym *newSynonym;
    char* tmpChar;
    int length;

    newSynonym = malloc(sizeof(struct TSynonym));
    if(newSynonym == NULL) {
        return NULL;
    }

    length = strlen(synonym->name);
    tmpChar = malloc(length * sizeof(char));
    if(tmpChar == NULL) {
        return NULL;
    }
    strcpy(tmpChar, synonym->name);
    newSynonym->name = tmpChar;

    newSynonym->type = synonym->type;

    newSynonym->dataType = synonym->dataType;

    newSynonym->paramCounter = synonym->paramCounter;

    newSynonym->inicialized = synonym->inicialized;

    switch(synonym->dataType) {
        case D_INT: 
            if(synonym->inicialized == true) newSynonym->value.integer = synonym->value.integer;
            break;
        case D_REAL:
            if(synonym->inicialized == true) newSynonym->value.real = synonym->value.real;
            break;
        case D_BOOL:
            if(synonym->inicialized == true) newSynonym->value.boolean = synonym->value.boolean;
            break;
        case D_STRING:
            if(synonym->inicialized == true) {
                newSynonym->value.string = malloc(sizeof(string));
                if(newSynonym->value.string == NULL) {
                    return NULL;
                }
                strInit(newSynonym->value.string);
                strCopyString(newSynonym->value.string, synonym->value.string);
            }
            break;
        case D_LABEL:
            newSynonym->value.label = synonym->value.label;
            break;
        case D_FUNC:
            length = strlen(synonym->value.function.funcParams);
            tmpChar = malloc(length * sizeof(char));
            if(tmpChar == NULL) {
                return NULL;
            }
            strcpy(tmpChar, synonym->value.function.funcParams);
            newSynonym->value.function.funcParams = tmpChar;

            newSynonym->value.function.instList = NULL;
            newSynonym->value.function.localTable = NULL;
            break;
        default:
            free(newSynonym);
            return NULL;
    }    
    return newSynonym;
}

/**
 * @brief Delete the table of symbols
 * @param *table Table of symbols
 */
bool DeleteTable(TSymTable *table) {
    TSynonym *tmpSynonym;
    bool rvalue;

    //Clear all lists in the table and the whole table
    if(table != NULL){
        for(int i = 0; i < table->sizeOfTable; i++) {
            while(table->columns[i] != NULL) {
                tmpSynonym = table->columns[i];
                table->columns[i] = table->columns[i]->nextSynonym;
                switch(tmpSynonym->dataType) {
                    case D_STRING:
                        if(((tmpSynonym->type == I_VAR) || (tmpSynonym->type == I_CONST)) && (tmpSynonym->inicialized == true)) {
                            rvalue = strClear(tmpSynonym->value.string);
                        }
                        break;
                    case D_FUNC:
                        if(tmpSynonym->value.function.instList != NULL) InstListDispose(tmpSynonym->value.function.instList);
                        if(tmpSynonym->value.function.instList != NULL) rvalue = DeleteTable(tmpSynonym->value.function.localTable);
                        break;
                    default:
                        break;    
                }
                free(tmpSynonym);
            }
        }
    }

    free(table->columns); 
    free(table);
    return rvalue;
}

/**
 * @brief Finds the determinated element in the table
 * @param *table Table of symbols
 * @param *nameOfToken Name of function, variable or constant
 * @return Pointer to found element. If not found, returns NULL
 */
TSynonym* SearchTable(TSymTable *table, char *nameOfToken) {
    int key;
    TSynonym *tmpSynonym; 

    if(table == NULL) return NULL;

    //Generate hash
    key = GetHash(nameOfToken, table->sizeOfTable);

    tmpSynonym = table->columns[key];
 
    //Search the list on the index, which was found by GetHash function   
    while(tmpSynonym != NULL) {
        if(strcmp(tmpSynonym->name, nameOfToken) != 0) {
            tmpSynonym = tmpSynonym->nextSynonym;
        }
        else {
            return tmpSynonym;
        }
    }
    return NULL;
}
/*******************************************************************************/
/******************************** Quick Sort ***********************************/
/*******************************************************************************/

/**
 * @brief Sorts a field
 * @param arr[] Array for sorting
 * @param left Index where sorting should start
 * @param right Index where sorting should end
 * Prevzato ze skript do predmetu IAL
 */
string* QuickSort(char arr[]) {
    char *tmpArr;
    int length;
    string *s = NULL;

    length = strlen(arr);
    tmpArr = malloc(length * sizeof(char));
    strcpy(tmpArr, arr);

    QSCalc(tmpArr, 0, length - 1);

    length = strlen(tmpArr);
    s = malloc(sizeof(string));
    strInit(s);
    for(int i = 0; i < length; i++) {
        strAddChar(s, tmpArr[i]);
    }

    return s;
}

/**
  * @brief Help funciton for QSort
  * @param arr[] Array for sorting
  * @param left Idex where sorting should start
  * @param right Index where sorting should end;
  */
void QSCalc(char arr[], int left, int right) {
    int i = 0, j = 0;

    Partition(arr, left, right, &i, &j);
    
    if(left < j) QSCalc(arr, left, j);
    if(i < right) QSCalc(arr, i, right); 
} 

/**
 * @brief Divides array into parts smaller and bigger then pseudomedian
 * @param arr[] Array for dividing
 * @param left Index where dividing should start 
 * @param right Index where dividing should end
 * @param *i Index for searching from left
 * @param *j Index for searching from right
 * Prevzato ze skript do predmetu IAL
 */
void Partition(char arr[], int left, int right, int *i, int *j) {
    int PM;
    *i = left;
    *j = right;
    int tmp;

    PM = arr[((*i) + (*j)) / 2];

    do {
        while(arr[*i] < PM) (*i)++;
        while(arr[*j] > PM) (*j)--;

        if(*i <= *j) {
            tmp = arr[*i];
            arr[*i] = arr[*j];
            arr[*j] = tmp;

            (*i)++;
            (*j)--; 
        }
    } while(*i < *j);
}

/*******************************************************************************/
/*********************************** BMA ***************************************/
/*******************************************************************************/

/**
 *@brief Returns higher value
 *@param a First value to compare
 *@param b Second value to compare
 */
int max(int a, int b) {
    return (a > b ? a : b);
}

/**
 *@brief Return smaller value
 *@param a First value to compare
 *@param b Second value to compare
 */
int min(int a, int b) {
    return (a < b ? a : b);
}

/**
 *@brief Computes the array of jumps for string
 *@param *pattern String which represents pattern
 *@param charJump[] Array for jumps
 */ 
void ComputeJumps(char *pattern, char charJump[]) {

    int m = strlen(pattern);

    for(int i = 0; i < 256; i++) {
        charJump[i] = m;
    }
    
    for(int k = 0; k < m - 1; k++) {
        charJump[(int) pattern[k]] = m - k - 1;
    }
}

/**
 *@brief Computes the arrey of jumps for string
 *@param *pattern String which represents pattern
 *@param matchJump[] Array for jumps
 */
void ComputeMatchJumps(char *pattern, int matchJump[]) {
    int k, q, qq, patternLength = strlen(pattern);
    int backup[patternLength + 1];

    for(k = 1; k <= patternLength; k++) {
        matchJump[k - 1] = 2 * patternLength - k;
    }

    k = patternLength;
    q = patternLength + 1;

    while(k > 0) {
        backup[k - 1] = q;

        while((q <= patternLength) && (pattern[k - 1] != pattern[q - 1])) {
            matchJump[q - 1] = min(matchJump[q - 1], patternLength - k);
            q = backup[q - 1];
        }

        k--;
        q--;
    }

    for(k = 1; k <= q; k++) {
        matchJump[k - 1] = min(matchJump[k - 1], patternLength + q - k);
    }

    qq = backup[q - 1];
    while(q <= patternLength) {
        while(q <= qq) {
            matchJump[q - 1] = min(matchJump[q - 1], qq - q + patternLength);
            q++;
        }
        qq = backup[qq - 1];
    }
}

/**
 *@brief Computes the index of string in text
 *@parama *pattern String we want to search
 *@param *text String for searching
 *@return Index to text
 */ 
int BMA(char *pattern, char *text) {
    int j, k, lengthPattern, lengthText;
    lengthPattern = strlen(pattern);
    lengthText = strlen(text);
    char charJump[255];
    int matchJump[lengthPattern];

    if(strcmp(pattern, "") == 0) return 1;

    ComputeJumps(pattern, charJump);
    ComputeMatchJumps(pattern, matchJump);

    j = lengthPattern;
    k = lengthPattern;

    while((j <= lengthText) && (k > 0)) {
        if(text[j - 1] == pattern[k - 1]) {
            j--;
            k--;
        }
        else {
            j = j + max(charJump[(int) text[j - 1]], matchJump[k - 1]);
            k = lengthPattern;
        }
    }

    return (k == 0) ?  (j + 1) : 0;
}
