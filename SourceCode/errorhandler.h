/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


/**
 * INCLUDE (MACRO) GUARD 
 */
#ifndef ERROR_HANDLER
#define ERROR_HANDLER

#include <stdio.h>
#include <stdlib.h>

/** 
 * @desc - Error values
 */
enum errorValue {

  E_OK = 0,
  E_LEXICAL,
  E_SYNTACTIC,
  E_SEMANTIC_VAR,
  E_SEMANTIC_EXP,
  E_SEMANTIC_OTHER,
  E_RT_NUM_VALUE,
  E_RT_UNINITIALIZED_VAR,
  E_RT_ZERO_DIV,
  E_RT,
  E_INTERNAL = 99,
};

/**
 * @decs direct linear list
 */
typedef struct {
  int err_row;
  int err_type;
} tErrorHandler;


/**
 * Functions of errorhandler.c
 * @function ErrorListInit, initialization, list of error messages
 * @function ErrorListFree, free allocated memory
 * @function ErrorListAdd, add error reporting, setting ERR_FLAG
 * @function ErrorListPrint, listing of error messages
 */
void ErrorInit( tErrorHandler * );
void ErrorAdd( tErrorHandler *, int row, int error );
void ErrorPrint( void );

#endif