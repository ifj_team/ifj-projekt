/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k   <<<<<<                              *
 *                                                                     *
 ***********************************************************************/


#ifndef IAL
#define IAL

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include "str.h"
#include "instlist.h"
#include "errorhandler.h"

typedef struct TSymTable TSymTable;

typedef struct func {
    char *funcParams;
    struct tInstList *instList;
    TSymTable *localTable;
} func;

//the real data type in c
typedef union {
    int integer;
    double real;
    bool boolean;
    string *string;
    struct tInst *label;
    func function;
} TDataValue;

//list of data types of stored items
typedef enum {D_INT, D_REAL, D_BOOL, D_STRING, D_LABEL, D_FUNC} TDataType;

//list of items that can be stored
typedef enum {I_VAR, I_CONST, I_FUNC, I_LABEL} TItemType;

//stucture of one table element
typedef struct TSynonym {
    char *name;
    TItemType type;
    TDataType dataType;
    TDataValue value;
    bool inicialized;
    int paramCounter;
    struct TSynonym *nextSynonym;
} TSynonym;


//structure of table
struct TSymTable {
    int sizeOfTable;
    int labConNumber;
    TSynonym **columns;
};

/**
 * @brief Function to generate hash key
 * @param *key Name of function, variable or constant 
 * @param sizeOfTable Size of table of symbols
 * @return 
 */
unsigned int GetHash(char *key, int sizeOfTable);

/**
 * @brief Inicialization of table of symbols
 * @param sizeOfTable Size of table of symbols
 * @return Pointer to new table
 */
TSymTable* InitTab(int sizeOfNewTable);

/**
 * @brief Insert a new element into the table
 * @param *table Table of symbols
 * @param *nameOfToken Name of function, variable, constant, label
 * @param itemType Type of item for saving (function, constant, label, variable)
 * @param dataType Data type of the item for saving (integer, real, boolean, string, label, function)
 * @param dataValue The actual value that is going to be stored
 * @return Pointer to new element
 */
TSynonym* NewSynonym(TSymTable *table, char *nameOfToken, TItemType itemType, TDataType dataType, TDataValue dataValue, bool init);

/**
 *@brief Copy table of Synonyms
 *@param *table Table which should be copied
 *@return Pointer to new table
 */
TSymTable* CopyTable(TSymTable *table);

/**
 *@brief Creates a copy of synonym;
 *@param *synonym Synonym which should be copied
 *@return Pointer to new synonym
 */
TSynonym* CopySynonym(TSynonym *synonym);

/**
 * @brief Delete the table of symbols
 * @param *table Table of symbols
 */
bool DeleteTable(TSymTable *table);

/**
 * @brief Finds the determinated element in the table
 * @param *table Table of symbols
 * @param *nameOfToken Name of function, variable or constant
 * @return Pointer to found element. If not found, returns NULL
 */
TSynonym* SearchTable(TSymTable *table, char *nameOfToken);

/**
 * @brief Sorts a field
 * @param arr[] Array for sorting
 * @param left Index where sorting should start
 * @param right Index where sorting should end
 * Prevzato ze skript do predmetu IAL
 */
string* QuickSort(char arr[]);

/**
 * @brief Sorts a field
 * @param arr[] Array for sorting
 * @param left Index where sorting should start
 * @param right Index where sorting should end
 * Prevzato ze skript do predmetu IAL
 */
void QSCalc(char arr[], int left, int right);

/**
 * @brief Divides array into parts smaller and bigger then pseudomedian
 * @param arr[] Array for dividing
 * @param left Index where dividing should start 
 * @param right Index where dividing should end
 * @param *i Index for searching from left
 * @param *j Index for searching from right
 * Prevzato ze skript do predmetu IAL
 */
void Partition(char arr[], int left, int right, int *i, int *j);

/**
 *@brief Computes the index of string in text
 *@parama *pattern String we want to search
 *@param *text String for searching
 *@return Index to text
 */ 
int BMA(char *pattern, char *text);

/**
 *@brief Computes the array of jumps for string
 *@param *pattern String which represents pattern
 *@param charJump[] Array for jumps
 */ 
void ComputeJumps(char *pattern, char charJump[]);

/**
 *@brief Computes the arrey of jumps for string
 *@param *pattern String which represents pattern
 *@param matchJump[] Array for jumps
 */
void ComuteMatchJumps(char *pattern, int matchJump[]);

/**
 *@brief Returns higher value
 *@param a First value to compare
 *@param b Second value to compare
 */
int max(int a, int b);

/**
 *@brief Return smaller value
 *@param a First value to compare
 *@param b Second value to compare
 */
int min(int a, int b);

/**
 *@brief Count the number of digits in integer
 *@param num Integer for counting
 *@return number of digits
 */
int numOfDigits(int num);
#endif
