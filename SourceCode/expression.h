/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00   <<<<<<                              *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <malloc.h>
#include <string.h>

#include "ial.h"
#include "errorhandler.h"
#include "scanner.h"
#include "instlist.h"


#define TABLE_SIZE 14 //Size od precedence table

#define RULES_COUNT 13 //count of precedent rules (etc. 1. E + E )

#define UNDEF_TYPE 42 //wrong operands type, undef result type


/**
 * @desc identify record in precedence table (<, >, =, ' ')
 */
typedef enum {
    P_L, //less <
    P_M, //more >
    P_E, //equal =
    P_X, //empty ' '
    P_F  //final state [$,$]
} TPrecItem;

/**
 * @desc identify type of input token to be pushed into stack
 * @desc coresponde with index in precedence table
 * @desc !!!Be carefull about changes, order matter!!!
 */
typedef enum { // P_ prefix beacause of conflict with scanner
    P_MUL = 0, // *
    P_DIV,     // /
    P_ADD,     // +
    P_SUB,     // -
    P_LESS,    // <
    P_MORE,    // >
    P_LESS_EQ, // <=
    P_MORE_EQ, // >=
    P_EQ,      // =
    P_DIFF,    // <>
    P_L_PAR,   // (
    P_R_PAR,   // )
    P_NUM,     // I (indicate all variable and constants, also TRUE, FALSE)
    P_DELIM,   // $

    //this isnt in prec table
    P_NONTERM,   // NONTERMINAL in stack
    P_HANDLE,    // symbol < in stack
    P_REDUCTION, // symbol > in input
} TOperator;

typedef enum {
    R_MUL = 0, 
    R_DIV,     // /
    R_ADD,     // +
    R_SUB,     // -
    R_LESS,    // <
    R_MORE,    // >
    R_LESS_EQ, // <=
    R_MORE_EQ, // >=
    R_EQ,      // =
    R_DIFF,    // <>
    R_PAR,     // (
    R_NUM,     // I (indicate all variable and constants, also TRUE, FALSE)
    R_ERR   // $
} TRule; //grammatic rule type for reduction

//Structure of one stack element
typedef struct TPrecStackElement {
    struct TPrecStackElement *next;
    
    TOperator operator;
    TSynonym *synonym;
} TPrecStackElement;

//Structure of stack
typedef struct TPrecStack {
    TPrecStackElement *top;
} TPrecStack;



int Expression(TSymTable *localSymTable, TSynonym **synonym, tInstList *instList);

TPrecItem GetAction(TOperator row, TOperator col);

int GetTopTerminal(TPrecStack *s, TPrecStackElement **e);

int PrepareTokenData(TSymTable *localSymTable, TPrecStackElement *e);

int PushHandleAfter(TPrecStack *s, TPrecStackElement *terminal);

int MakeReduction(TPrecStack *s, TSynonym **synonym);

TOperator TokenToOperator();

TDataType GetResultType(TSynonym *operand1, TSynonym *operand2, TRule rule);

iInstruction GetInstruction(int rule);


/***********************************************************
                  PRECEDENT STACK PART
************************************************************/


/**
 * @brief Inicializaton of stack
 * @param *stack Pointer to stack
 */
void InitPStack(TPrecStack *stack);

/**
 * @brief Push one element to the stack
 * @param *stack Pointer to stack
 * @param Element which should be put into the stack
 * @return True if everything is OK, false if not
 */
bool PushPStack(TPrecStack *stack, TPrecStackElement element);

/**
 * @brief Pop one element from the stack
 * @param *stack Pointer to stack
 * @return True if everything is OK, false if not
 */
bool PopPStack(TPrecStack *stack);

/**
 * @brief Read one element from top of the stack
 * @param *stack Pointer to stack
 * @return Pointer to element on top of the stack
 */
TPrecStackElement* TopPStack(TPrecStack *stack);

/**
 * @brief Finds out, if the stack is empty or not
 * @param *stack Pointer to stack
 * @return Tru if empty, false if not   
 */
bool EmptyPStack(TPrecStack *stack);

/**
 * @brief Deletes the stack
 * @param Pointer to stack
 */
void DeletePStack(TPrecStack *stack);


#endif