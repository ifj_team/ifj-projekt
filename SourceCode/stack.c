/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k   <<<<<<                              *
 *                                                                     *
 ***********************************************************************/


#include "stack.h"

/**
 * @brief Inicializaton of stack
 * @param *stack Pointer to stack
 */
void InitStack(TStack *stack) {
    stack->top = NULL;
}

/**
 * @brief Push one element to the stack
 * @param *stack Pointer to stack
 * @param Element which should be put into the stack
 * @return True if everything is OK, false if not
 */
bool PushStack(TStack *stack, TStackElement element) {
    if(stack == NULL) return false;

    TStackElement *newElement;

    newElement = malloc(sizeof(TStackElement));
    if(newElement == NULL) return false;

    newElement->next = stack->top;

    stack->top = newElement;

    newElement->table= element.table;

    return true;
}

/**
 * @brief Pop one element from the stack
 * @param *stack Pointer to stack
 * @return True if everything is OK, false if not
 */
bool PopStack(TStack *stack) {
    if(stack == NULL) return false;

    TStackElement *delElement;

    if(stack->top != NULL) {
        delElement = stack->top;

        stack->top = stack->top->next;

        free(delElement);

        return true;
    }
    return false;
}

/**
 * @brief Read one element from top of the stack
 * @param *stack Pointer to stack
 * @return Pointer to element on top of the stack
 */
TStackElement* TopStack(TStack *stack) {
    if(stack == NULL) return NULL;
   
    return  stack->top; 
}

/**
 * @brief Finds out, if the stack is empty or not
 * @param *stack Pointer to stack
 * @return Tru if empty, false if not   
 */
bool EmptyStack(TStack *stack) {
    return (stack->top == NULL) ? true : false;
}

/**
 * @brief Deletes the stack
 * @param Pointer to stack
 */
void DeleteStack(TStack *stack) {
    bool tmp = true;
    
    while (tmp != false) {
        tmp = PopStack(stack);
    }
    free(stack);
}
