/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05   <<<<<<                              *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include "str.h"
#include "instlist.h"
#include "scanner.h"
#include "errorhandler.h"
#include "stack.h"
#include "ial.h"
#include "expression.h"


TSynonym *FindParam(TSymTable *hash, int index);
int Parse();
int GlobalVariable(bool first);
int FunctionDefinition();
int FunctionParameter(bool first, string *funcParam, TSymTable *hash, bool insert, int counter);
int MainBody();
int CompoundStatement(bool mainBody, tInstList *ilist);
int Statement(bool first, tInstList *ilist, bool assign);
int Assignment(TSynonym *variable, tInstList *ilist);
int Function(tInstList *ilist, TSynonym *idFunction, TSynonym *variable);
int If(tInstList *ilist);
int While(tInstList *ilist);
int Readln(tInstList *ilist);
int BuiltIn(tInstList *ilist, TSynonym *destination);
int AllFunctionDefined();
TSynonym *FindVariable(char *Id);
#endif
