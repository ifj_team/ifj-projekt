/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#include "errorhandler.h"

extern tErrorHandler error;	/* ERROR HANDLER */
extern int ERR_FLAG;		/* ERROR FLAG */

/**
 * @desc - Error messages
 */
const char const *err_msg[] = {

  [E_OK] = 			"OK",
  [E_LEXICAL] = 		"[error 1] program error, lexical analysis.\n",
  [E_SYNTACTIC] = 		"[error 2] program error, parsing.\n",
  [E_SEMANTIC_VAR] = 		"[error 3] program error, semantic error - undefined function or variable.\n",
  [E_SEMANTIC_EXP] = 		"[error 4] program error, semantic error - type compatibility.\n",
  [E_SEMANTIC_OTHER] = 		"[error 5] program error, other semantic error.\n",
  [E_RT_NUM_VALUE] = 		"[error 6] runtime error, retrieving value from the input.\n",
  [E_RT_UNINITIALIZED_VAR] = 	"[error 7] runtime error, uninitialized variable.\n",
  [E_RT_ZERO_DIV] = 		"[error 8] runtime error, divide by zero.\n",
  [E_RT] = 			"[error 9] runtime error.\n",
  [E_INTERNAL] = 		"[error 99] internal program error.\n",
};

/**
 * @desc - Initialization, list of error messages
 */
void ErrorInit( tErrorHandler *EL ) {
  
  EL->err_row = 0;
  EL->err_type = 0;
  
}

/**
 * @desc add error reporting, setting ERR_FLAG
 * @param row line contains error
 * @param error type of error
 */
void ErrorAdd( tErrorHandler *EL, int row, int err ) {
  
  if( ERR_FLAG ) printf("Critical Error!\n");
  
  ERR_FLAG = err;
  EL->err_row = row;
  EL->err_type = err;
} 

/**
 * @desc listing of error messages
 */
void ErrorPrint( void ) {  
  if( ERR_FLAG ) {
      if( ERR_FLAG == E_INTERNAL )
	  fprintf(stderr,"[error] %s",err_msg[error.err_type]);
      else
	  fprintf(stderr,"[Line %d]%s ",error.err_row,err_msg[error.err_type]);
  }
}