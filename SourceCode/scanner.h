/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01   <<<<<<                              *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#ifndef SCANNER_H
#define SCANNER_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "str.h"
#include "errorhandler.h"

/*
 * Line counter
 */
extern int LINE_CNT;

/*
 * Token struct
 */
typedef struct {
	int tok; 	 // Token number
	string key; // structure string
	int inum;    // integer number
	double dnum;    // double number
} token;

#define APOS 39 // ASCII value of apostrophe
#define NL 10 // ASCII value of new line

/*
 * TOKEN DEFINITIONS
 *
 * Identifiers
 */
#define ID 400
/* 
 * Key words
 */
#define BEGIN 100
#define BOOLEAN 101
#define DO 102
#define ELSE 103
#define END 104
#define FFALSE 105
#define FIND 106
#define FORWARD 107
#define FUNCTION 108
#define IF 109
#define INTEGER 110
#define READLN 111
#define REAL 112
#define SORT 113
#define STRING 114
#define THEN 115
#define TTRUE 116
#define VAR 117
#define WHILE 118
#define WRITE 119
#define ENDDOT 120
/*
 * Operators, special characters
 */
#define COLON 200   // :
#define ASSIGN 201  // :=
#define EQ 202      // =
#define SEMICOL 203 // ;
#define LBR 204     // (
#define RBR 205     // )
#define COMMA 206   // ,
#define PLUS 207    // +
#define MINUS 208   // -
#define MULT 209    // *
#define DIV 210     // /
#define SMLL 211    // <
#define SMLLEQ 212  // <=
#define DIFF 213    // <>
#define BGG 214     // >
#define BGGEQ 215   // >=
#define ENDFILE 216 // end of file
#define FETCH 420   // no token recognized
/**
 * String literal
 */
#define STR 300
/**
 * Numbers
 */
#define DOUBLE 301  // decimal number
#define INT 302     // integer

/**
 * Functions of scanner.c
 * @function SetSourceFile - saves input file into global variable
 * @function GetToken - determine the type of token
 */
void SetSourceFile(FILE *f);
int GetToken(string *attr);

#endif
