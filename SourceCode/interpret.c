/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00                                       *
 *     Sokac Tomas    - xsokac00   <<<<<<                              *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#include "interpret.h"

extern int LINE_CNT;		/* LINE COUNTER */
extern int ERR_FLAG;		/* ERROR FLAG */
extern tInstList ilist;		/* INSTRUCTION LIST */
extern tErrorHandler error;	/* ERROR HANDLER */

/**
 * @desc manages all activities of Interpret
 * @param *L Instructions List
 * @return boolean
 */
bool Interpret( tInstList *L ) {
  printf("%d ---------------\n",L->first->type);
  /* instruction list exist */
  if( !L->first ) return false;
  /* set first instruction as active */
  InstListSetFirst(L);
  /* active instruction */
  tInstPtr ptr;
  /* char used in Instruction working with string */
  char c;
  /* Quick sort */
  string* QuickSorted;
  /* Copy index and counted */
  int index;
  int counted;

  /**
   * Instruction list is not empty
   * program runs without error 
   */
  while ( true ) {
	
	ptr = L->active;
	/* Type of instruction */
	switch ( ptr->type ) {
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 0, I_STOP, end (NULL,NULL,NULL) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_STOP:
		    return true;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 1, I_READ, read (NULL,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_READ:
		    switch( ptr->dest->dataType ) {
			/* read integer */
			case D_INT:
			    if ( !scanf("%d",&(ptr->dest->value.integer)) ) {
				/* ERROR 6, RUNTIME ERROR  */
				ErrorAdd(&error,LINE_CNT,E_RT_NUM_VALUE);
				break;
			    }
			    ptr->dest->inicialized = true;
			    break;
			/* read simple float or double and return it as the shorter of the two */
			case D_REAL:
			    if ( !scanf("%lf",&(ptr->dest->value.real)) ) {
				/* ERROR 6, RUNTIME ERROR */
				ErrorAdd(&error,LINE_CNT,E_RT_NUM_VALUE);
				break;
			    }
			    ptr->dest->inicialized = true;
			    break;
			/* read string */
			case D_STRING:
			    /* clear string */
			    if( !strClear(ptr->dest->value.string) ) {
				/* MALLOC */
				if( (ptr->dest->value.string = malloc(sizeof(string))) != NULL ) {
				    strInit(ptr->dest->value.string);
				
				    while((c = getchar()) != '\n' && !ERR_FLAG ) {
					/* end of file */
					if( c == EOF ) {
					    /* ERROR 6, RUNTIME ERROR */
					    ErrorAdd(&error,LINE_CNT,E_RT_NUM_VALUE);
					    break;
					}
					strAddChar(ptr->dest->value.string, c);
				    }
				    ptr->dest->inicialized = true;
				    break;
			    }
			    }
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 2, I_WRITE, write (src1,NULL,NULL)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_WRITE:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* print integer */
			case D_INT:
			    printf("%d",ptr->src1->value.integer);
			    break;
			/* print real */
			case D_REAL:
			    printf("%g",ptr->src1->value.real);
			    break;
			/* print string */
			case D_STRING:
			    printf("%s",ptr->src1->value.string->str);
			    break;
			/* print boolean */
			case D_BOOL:
			    printf("%s", ptr->src1->value.boolean ? "TRUE" : "FALSE");
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 3, I_ADD, addition (int/real) concatenation (string) (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_ADD:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->dest->dataType ) {
			/* result is integer */
			case D_INT:
			    ptr->dest->value.integer = ptr->src1->value.integer + ptr->src2->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			/* result is real */
			case D_REAL:
			    ptr->dest->value.real = 
			    /* first operand can be real or integer */
			    ( (ptr->src1->dataType == D_REAL)?(ptr->src1->value.real):(ptr->src1->value.integer) )
			    /* second operand can be real or integer */
			    + ( (ptr->src2->dataType == D_REAL)?(ptr->src2->value.real):(ptr->src2->value.integer) );
			    ptr->dest->inicialized = true;
			    break;
			/* CONCATENATION */
			case D_STRING:
			    /* MALLOC */
			    if( ( ptr->dest->value.string = malloc(sizeof(string))) != NULL ) {
				strInit(ptr->dest->value.string);
				/* copy src1 to destination */
				strCopyString(ptr->dest->value.string,ptr->src1->value.string);
				/* add src 1 to src2 (in destination) */
				strAddStr(ptr->dest->value.string,ptr->src2->value.string);
				ptr->dest->inicialized = true;
			    }
			    break;
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 4, I_SUB, subtraction (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_SUB:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->dest->dataType ) {
			/* result is integer */
			case D_INT:
			    ptr->dest->value.integer = ptr->src1->value.integer - ptr->src2->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			/* result is real */
			case D_REAL:
			    ptr->dest->value.real = 
			    /* first operand can be real or integer */
			    ( (ptr->src1->dataType == D_REAL)?(ptr->src1->value.real):(ptr->src1->value.integer) )
			    /* second operand can be real or integer */
			    - ( (ptr->src2->dataType == D_REAL)?(ptr->src2->value.real):(ptr->src2->value.integer) );
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 5, I_MUL, multiply (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_MUL:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->dest->dataType ) {
			/* result is integer */
			case D_INT:
			    ptr->dest->value.integer = ptr->src1->value.integer * ptr->src2->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			/* result is real */
			case D_REAL:
			    ptr->dest->value.real = 
			    /* first operand can be real or integer */
			    ( (ptr->src1->dataType == D_REAL)?(ptr->src1->value.real):(ptr->src1->value.integer) )
			    /* second operand can be real or integer */
			    * ( (ptr->src2->dataType == D_REAL)?(ptr->src2->value.real):(ptr->src2->value.integer) );
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 6, I_DIV, division (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_DIV:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src2->dataType ) {
			/* divisor is integer */
			case D_INT:
			    /* condition division by zero */
			    if( ptr->src2->value.integer != 0 ) {
				ptr->dest->value.real = (ptr->src1->dataType == D_INT? ptr->src1->value.integer: ptr->src1->value.real) / ptr->src2->value.integer;
				ptr->dest->inicialized = true;
				break;
			    }
			    /* ERROR 8, RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT_ZERO_DIV);
			    break;
			/* divisor is real */
			case D_REAL:
			    /* condition division by zero */
			    if( ptr->src2->value.real != 0.0 ) {
				ptr->dest->value.real = (ptr->src1->dataType == D_REAL? ptr->src1->value.real: ptr->src1->value.integer) / ptr->src2->value.real;
				ptr->dest->inicialized = true;
				break;
			    }
			    /* ERROR 8, RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT_ZERO_DIV);
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 7, I_NOT, negative (src1,NULL,NULL) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_NOT:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    ptr->src1->value.boolean = !ptr->src1->value.boolean;
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 8, I_LESS, less than (src1,src2,dest)
		 * -------------------------------------------------------------------------------------------- * 
		 */
		case I_LESS:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer < ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real < ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) < 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean < ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 9, I_LESS_EQL, less than or equal (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_LESS_EQL:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer <= ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real <= ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) <= 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean <= ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 10, I_GREATER, greater than (src1,src2,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_GREATER:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer > ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real > ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) > 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean > ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 11, I_GREATER_EQL, greater than or equal (src1,src2,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_GREATER_EQL:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer >= ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real >= ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) >= 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean >= ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 12, I_EQL, equal (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_EQL:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer == ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real == ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) == 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean == ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 13, I_DIFF, different (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_DIFF:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			/* result is boolean */
			case D_INT:
			    ptr->dest->value.boolean = ptr->src1->value.integer != ptr->src2->value.integer ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.boolean = ptr->src1->value.real != ptr->src2->value.real ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.boolean = ( strCmpString(ptr->src1->value.string, ptr->src2->value.string) != 0 ? true: false );
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean != ptr->src2->value.boolean ? true: false;
			    ptr->dest->inicialized = true;
			    break;
			/* default */    
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		        
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 14, I_FIND, BMA (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_FIND:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized || !ptr->src2->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    ptr->dest->value.integer = BMA(ptr->src2->value.string->str,ptr->src1->value.string->str);
		    ptr->dest->inicialized = true;
		    break;
		
		/*  
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 15, I_SORT, QuickSort (src1,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_SORT:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    /* temp pointer */
		    /* MALLOC TEMP */
		    if( ( QuickSorted = malloc(sizeof(string))) != NULL ) 
			strInit(QuickSorted);
		    /* destination pointer */
		    /* MALLOC */
		    if( ( ptr->dest->value.string = malloc(sizeof(string))) != NULL ) {
			/* STRING INIT */
			strInit(ptr->dest->value.string);
			/* Quick sort */
			QuickSorted = QuickSort( ptr->src1->value.string->str );
			/* copy string to result destination */
			strCopyString(ptr->dest->value.string, QuickSorted);
			/* FREE TEMP string */
			free(QuickSorted);
			ptr->dest->inicialized = true;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 16, I_LEN, length  (src1,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_LEN:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    ptr->dest->value.integer = strGetLength(ptr->src1->value.string);
		    ptr->dest->inicialized = true;
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 17, I_COPY, copy (src1,src2,dest) 
		 * -------------------------------------------------------------------------------------------- *
		 */    
		case I_COPY:
		    /* INICIALIZATION */
		    if( ptr->src2 ) {
			if( !ptr->src1->inicialized || !ptr->src2->inicialized) {
			    /* ERROR 7, RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			    break;
			}
		    }
		    else {
			if( !ptr->src1->inicialized ) {
			    /* ERROR 7, RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			    break;
			}
		    }
		    if( !ptr->dest ) {
			index = ptr->src1->value.integer;
			counted = ptr->src2->value.integer;
		    }
		    else {
			/* destination pointer */
			/* MALLOC */
			if( ( ptr->dest->value.string = malloc(sizeof(string))) != NULL ) {
			    /* STR INIT */
			    strInit(ptr->dest->value.string);
			    ptr->dest->value.string = Copy(ptr->src1->value.string, index, counted );
			    ptr->dest->inicialized = true;
			}
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 18, I_LAB, label (NULL,NULL,NULL) 
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_LAB:
		    /* create label */
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 19, I_JUMP, jump / conditional jump (label) (src1,NULL,dest) / (NULL,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_JUMP:
		    /* JUMP IN THE SAME INSTRUCTION LIST */
		    if( !ptr->src2 ){
			/* CONDITIONAL JUMP */
			if( ptr->src1 != NULL ) {
			    if( ptr->src1->dataType == D_BOOL && !ptr->src1->value.boolean ) 
				break;
			}
			/* JUMP */
			L->active = ptr->dest->value.label;
			break;
		    }
		
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 20, I_MOV, move from src1 to dest (src1,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_MOV:
		    /* INICIALIZATION */
		    if( !ptr->src1->inicialized ) {
			/* ERROR 7, RUNTIME ERROR */
			ErrorAdd(&error,LINE_CNT,E_RT_UNINITIALIZED_VAR);
			break;
		    }
		    switch( ptr->src1->dataType ) {
			case D_INT:
			    ptr->dest->value.integer = ptr->src1->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.real = ptr->src1->value.real;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.string = ptr->src1->value.string;
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src1->value.boolean;
			    ptr->dest->inicialized = true;
			    break;
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 21, I_FRAME, create frame (src1,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_FRAME:
		    /* NULL */
		    if( !ptr->src1 ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* CREATE FRAME */
		    ptr->dest->value.function.localTable = CopyTable(ptr->src1->value.function.localTable);
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 22, I_CIL, copy Instructions List (src1,NULL,dest)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_CIL:
		    /* NULL */
		    if( !ptr->src1 ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* COPY INSTRUCTIONS LIST */
		    ptr->dest->value.function.instList = InstListCopy(ptr->src1->value.function.instList);
		    break;
		 
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 23, I_ILS, Instruction list subtitute (src1,src2,NULL)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_ILS:
		    /* NULL */
		    if( !ptr->src1 || !ptr->src2 ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* CREATE FRAME, RUNTIME ERROR */
		    ptr->src1->value.function.instList = InstListSubstitute(ptr->src1->value.function.instList, ptr->src2->value.function.localTable);
		    break;
		    
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 23, I_ILS, Instruction list subtitute (src1,src2,NULL)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_SET:
		    /* NULL */
		    if( !ptr->src1 || !ptr->src2 || !ptr->dest ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* SET PARAM */
		    ptr->dest = FindParam(ptr->src1->value.function.localTable, ptr->dest->paramCounter);
		    switch( ptr->src2->dataType ) {
			case D_INT:
			    ptr->dest->value.integer = ptr->src2->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.real = ptr->src2->value.real;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.string = ptr->src2->value.string;
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = ptr->src2->value.boolean;
			    ptr->dest->inicialized = true;
			    break;
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		
		/* 
		 * -------------------------------------------------------------------------------------------- *
		 * Instruction 23, I_ILS, Instruction list subtitute (src1,src2,NULL)
		 * -------------------------------------------------------------------------------------------- *
		 */
		case I_RET:
		    /* NULL */
		    if( !ptr->src1 ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* SET PARAM */
		    TSynonym* temp = FindParam(ptr->src1->value.function.localTable, -1);
		    switch( temp->dataType ) {
			case D_INT:
			    ptr->dest->value.integer = temp->value.integer;
			    ptr->dest->inicialized = true;
			    break;
			case D_REAL:
			    ptr->dest->value.real = temp->value.real;
			    ptr->dest->inicialized = true;
			    break;
			case D_STRING:
			    ptr->dest->value.string = temp->value.string;
			    ptr->dest->inicialized = true;
			    break;
			case D_BOOL:
			    ptr->dest->value.boolean = temp->value.boolean;
			    ptr->dest->inicialized = true;
			    break;
			/* default */
			default:
			    /* OTHER RUNTIME ERROR */
			    ErrorAdd(&error,LINE_CNT,E_RT);
			    break;
		    }
		    break;
		    
		    /* 
		    * -------------------------------------------------------------------------------------------- *
		    * Instruction 23, I_ILS, Instruction list subtitute (src1,src2,NULL)
		    * -------------------------------------------------------------------------------------------- *
		    */
		case I_REC:
		    /* NULL */
		    if( !ptr->src1->value.function.instList ) ErrorAdd(&error,LINE_CNT,E_RT);
		    /* CREATE FRAME, RUNTIME ERROR */
		    Interpret(ptr->src1->value.function.instList);
		    break;
		    
		/* default */
		default:
		    /* OTHER RUNTIME ERROR */
		    ErrorAdd(&error,LINE_CNT,E_RT);
		    break;
	}
	if( ERR_FLAG ) return false;
	/* shift activity to next Instruction */
	InstListSucc(L);
	/* activity test */
	if( !InstListActive(L) ) return false;
  }
}

/**
 * @desc substitute of Instructions List
 * @param *L Instructions List
 * @param *T Local Symbolic Table
 * @return boolean
 */
tInstList* InstListSubstitute( tInstList *L, TSymTable *T ){
    /* temp symbolic table pointer */
    TSynonym* tmpTPtr;
    /* exit if pointer to instructions list or symbolic table is equal NULL */
    if( !L->first || !T ) return NULL;
    
    /* set first item as active */
    L->active = L->first;
    
    /* list activity */
    while( L->active ) {
	if ((tmpTPtr = SearchTable(T, L->active->src1->name)) != NULL ) L->active->src1 = tmpTPtr;
	if ((tmpTPtr = SearchTable(T, L->active->src2->name)) != NULL ) L->active->src2 = tmpTPtr;
	if ((tmpTPtr = SearchTable(T, L->active->dest->name)) != NULL ) L->active->dest = tmpTPtr;
	/* shift activity */
	L->active = L->active->nextItem;
    }
    return L;
}
