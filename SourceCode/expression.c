/***********************************************************************
 *                                                                     *
 * Formalni jazyky a prekladace IFJ - Projekt                          *
 *                                                                     *
 * Brno University of technology - Faculty of Information Technology   *
 * Academic year: 2014/2015                                            *
 *                                                                     *
 * Team:                           Authors:                            *
 *     Dolinsky Milos - xdolin01                                       *
 *     Mencner Pavel  - xmencn00   <<<<<<                              *
 *     Sokac Tomas    - xsokac00                                       *
 *     Sommer Pavel   - xsomme05                                       *
 *     Svoboda Ondrej - xsvobo0k                                       *
 *                                                                     *
 ***********************************************************************/


#include "expression.h"

extern int ERR_FLAG;		/* ERROR FLAG */
extern int LINE_CNT;		/* LINE COUNTER */
extern tErrorHandler error;	/* ERROR HANDLER */
extern TSymTable *sTable;   /* GLOBAL SYMBOL TABLE */
tInstList *exprIList;           /* INSTRUCTION LIST, global but not extern, parser give us this in each call*/
extern token ParsToken;     /* ACTUAL PARSED TOKEN */


/**
 * @desc Table of precedence analysis - Global var
 * @desc use func GetAction() to access
 * @desc Less, More, Equal, X - empty
 */
const TPrecItem precTable[TABLE_SIZE][TABLE_SIZE] = {
//            *    /    +    -    <    >    <=   >=   =    <>   (    )    i    $
/*  *  */  { P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  /  */  { P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },

/*  +  */  { P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  -  */  { P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },

/*  <  */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  >  */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  <= */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  >= */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  =  */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },
/*  <> */  { P_L, P_L, P_L, P_L, P_M, P_M, P_M, P_M, P_M, P_M, P_L, P_M, P_L, P_M },

/*  (  */  { P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_E, P_L, P_X },
/*  )  */  { P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_X, P_M, P_X, P_M },
/*  i  */  { P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_M, P_X, P_M, P_X, P_M },
/*  $  */  { P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_L, P_X, P_L, P_F }
};

/**
 * @desc Table with gramatic rules for the expressions
 */
const TOperator precRules[RULES_COUNT][3] = {
/* R_MUL */     {P_NONTERM, P_MUL,     P_NONTERM}, // E * E    -
/* R_DIV */     {P_NONTERM, P_DIV,     P_NONTERM}, // E / E    |
/* R_ADD */     {P_NONTERM, P_ADD,     P_NONTERM}, // E + E    |
/* R_SUB */     {P_NONTERM, P_SUB,     P_NONTERM}, // E - E    |
/* R_LESS */    {P_NONTERM, P_LESS,    P_NONTERM}, // E < E    |
/* R_MORE */    {P_NONTERM, P_MORE,    P_NONTERM}, // E > E     ===>>>> E
/* R_LESS_EQ */ {P_NONTERM, P_LESS_EQ, P_NONTERM}, // E <= E   |
/* R_MORE_EQ */ {P_NONTERM, P_MORE_EQ, P_NONTERM}, // E >= E   |
/* R_EQ */      {P_NONTERM, P_EQ,      P_NONTERM}, // E = E    |
/* R_DIFF */    {P_NONTERM, P_DIFF,    P_NONTERM}, // E <> E   |
/* R_PAR */     {P_L_PAR,   P_NONTERM, P_R_PAR},   // ( E )    |
/* R_NUM */     {P_DELIM,   P_DELIM,   P_NUM},     // i        -
/* R_ERR */     {P_DELIM,   P_DELIM,   P_DELIM}    // Error detection
};


/**
 * @desc The main function of precedence analysis
 * @desc its called by parser while expression occurs
 * @param localSymTable pointer to local symbol table (in some function)
 * @param **synonym pointer to final synonym, where will be result after inerpretation
 */
int Expression(TSymTable *localSymTable, TSynonym **synonym, tInstList *instList) {
    
    exprIList = instList; //init of global with inst list

    string s; //string for scanner
    if (strInit(&s) != E_OK)
	    return E_INTERNAL;

    
    TPrecStack *pStack = (TPrecStack*) malloc(sizeof(TPrecStack)); //stack to save terms and NONterms
    InitPStack(pStack);

    TPrecStackElement newElement; //terminal we actually get from scanner, ready to process
    newElement.next = NULL;
    newElement.operator = P_DELIM;
    newElement.synonym = NULL;

    TPrecStackElement *topTerminal = NULL; //pointer to top TERMINAL on stack

    TPrecItem action; //to save what action we will do in next step

    TSynonym *tempSynonym = NULL; //to save result of expr, which finally return to parser

    int result = E_OK; //to save return code of func in cause of error 

    
    //////////////// HERE STARTS SOME CODE //////////////////

    PushPStack(pStack, newElement); //push delimiter $ to stack
    
    do {
        
        if( (result = GetTopTerminal(pStack, &topTerminal)) != E_OK )
            break;
        
        if( (result = PrepareTokenData(localSymTable, &newElement)) != E_OK )
            break;

        if (topTerminal == NULL) {//may not happen, just secure
            DeletePStack(pStack);
            return E_INTERNAL;
        }

        action = GetAction(topTerminal->operator, newElement.operator); //get what action we will do from the table

        switch(action) {
            case P_L: // < 
                if( (result = PushHandleAfter(pStack, topTerminal)) != E_OK ) //push < (handler) !after top terminal!
                    break;

                PushPStack(pStack, newElement); //push new terminal !at top!
                
                result = GetToken(&s); //Get next token from scanner
                break;
            ////////////////////////////////////////////////////////////////////////////////
            case P_M: // >
                result = MakeReduction(pStack, &tempSynonym);
                break;
            ////////////////////////////////////////////////////////////////////////////////
            case P_E: // =, 
                PushPStack(pStack, newElement); //push new terminal !at top!
                
                result = GetToken(&s); //Get next token from scanner
                break;
            ////////////////////////////////////////////////////////////////////////////////
            case P_F: //:-) [$, $] success of precedent analysis!
                break;
            ////////////////////////////////////////////////////////////////////////////////
            case P_X: //empty, Error
            default: //may not happen
                result = E_SYNTACTIC;
            ////////////////////////////////////////////////////////////////////////////////
        }

    } while ( (result == E_OK) && ((topTerminal->operator != P_DELIM) || (newElement.operator != P_DELIM)) );

    *synonym = tempSynonym;

    DeletePStack(pStack);
    return result;
}


/**
 * @desc Get type of action from precedent table (<, >, =, '')
 * @desc takes care about valid indexes
 */
TPrecItem GetAction(TOperator row, TOperator col) {
    if (row > P_DELIM || col > P_DELIM) 
        return P_X;
    else
        return precTable[row][col];
}


/**
 * @desc find and return first terminal on stack
 * @desc after: stack s should be the same, e has pointer to terminal on stack
 * @desc        or ERR_FLAG set if something unexpected happend
 * @param *s pointer to stack where we search
 * @param *o there we return found element
 */
int GetTopTerminal(TPrecStack *s, TPrecStackElement **e) {
    
    TPrecStackElement *tempElem = NULL;

    TPrecStack *tempStack = (TPrecStack*) malloc(sizeof(TPrecStack));
    InitPStack(tempStack);
    
    while ( (tempElem = TopPStack(s)) != NULL ) { //move nonterminals to tempStack until terminal comes
        
        PushPStack(tempStack, *tempElem);
        PopPStack(s);
        
        if (TopPStack(tempStack)->operator != P_NONTERM)
        	break;
    }
    
    //first terminal is now on top of tempStack
    
    if( TopPStack(tempStack) == NULL || TopPStack(tempStack)->operator == P_NONTERM ) { //may not happen (ever)
    	*e = NULL;
        DeletePStack(tempStack);
        return E_INTERNAL; //at least $ term should be always on stack, this is unexpected
    }

    tempElem = TopPStack(tempStack); //move terminal back to main stack and return pointer to it
    PushPStack(s, *tempElem);
    PopPStack(tempStack);
    *e = TopPStack(s);

    while ( (tempElem = TopPStack(tempStack)) != NULL ) {
    	//return stacks data 
        PushPStack(s, *tempElem);
        PopPStack(tempStack);

    }

    DeletePStack(tempStack);
    return E_OK;
}


/**
 * @desc Takes input token and make TPrecStackElement for him
 * @desc find or make synonym in symbol table
 * @desc -Check variables declarations (symbol tables)
 * @desc -Process constants (makes synonym)
 * @desc -catch unexpected tokens (if expr ends, catch this situation)
 */
int PrepareTokenData(TSymTable *localSymTable, TPrecStackElement *e) {

    TDataValue value; //union to save token value

    e->operator = TokenToOperator();

    if( e->operator == P_NUM) { //some constant or var to process
    	
        ///////////////////////////////////////////////////////////////////////        
        if (ParsToken.tok == ID) { //Identifer of some variable
            if ( (e->synonym = SearchTable(sTable, ParsToken.key.str)) != NULL ) //search id in global symTable
                return E_OK;

            if ( (e->synonym = SearchTable(localSymTable, ParsToken.key.str)) != NULL ) //search id in local symTable
                return E_OK;

            return E_SEMANTIC_VAR; //undeclared variable
        }
        ///////////////////////////////////////////////////////////////////////
        else if (ParsToken.tok == INT) { //Integer const value
            
            value.integer = ParsToken.inum;

            if( (e->synonym = NewSynonym(sTable, NULL, I_CONST, D_INT, value, true)) == NULL)
                return E_INTERNAL;

            e->synonym->inicialized = true;
        }
        ///////////////////////////////////////////////////////////////////////
        else if (ParsToken.tok == DOUBLE) { //Double (Real) const value
            
            value.real = ParsToken.dnum;

            if( (e->synonym = NewSynonym(sTable, NULL, I_CONST, D_REAL, value, true)) == NULL)
                return E_INTERNAL;

            e->synonym->inicialized = true;
        }
        ///////////////////////////////////////////////////////////////////////
        else if (ParsToken.tok == STR) { //String literal
            
            value.string = &(ParsToken.key);

            if( (e->synonym = NewSynonym(sTable, NULL, I_CONST, D_STRING, value, true)) == NULL)
                return E_INTERNAL;

            e->synonym->inicialized = true;
        }
        ///////////////////////////////////////////////////////////////////////
        else if (ParsToken.tok == TTRUE || ParsToken.tok == FFALSE) { //true, false keywords
            
            if (ParsToken.tok == TTRUE)
                value.boolean = true;
            else
                value.boolean = false;

            if( (e->synonym = NewSynonym(sTable, NULL, I_CONST, D_BOOL, value, true)) == NULL)
                return E_INTERNAL;

            e->synonym->inicialized = true;
        }
    }
    else { //others dont need to have synonym, just operators
        e->synonym = NULL;
    }
    
    return E_OK;
}


/**
 * @desc convert scanner notation of tokens to expression notation
 * @return actual operator
 */
TOperator TokenToOperator() {
    switch(ParsToken.tok) {
        case MULT:
            return P_MUL;
        case DIV:
            return P_DIV;
        case PLUS:
            return P_ADD;
        case MINUS:
            return P_SUB;
        case SMLL:
            return P_LESS;
        case BGG:
            return P_MORE;
        case SMLLEQ:
            return P_LESS_EQ;
        case BGGEQ:
            return P_MORE_EQ;
        case EQ:
            return P_EQ;
        case DIFF:
            return P_DIFF;
        case LBR:
            return P_L_PAR;
        case RBR:
            return P_R_PAR;
        case ID:
            return P_NUM; //id
        case DOUBLE:
        case INT:
        case STR: //string literal, concatenate and <, >...
            return P_NUM; //const
        case TTRUE:
        case FFALSE:
            return P_NUM; //true false, key words

        default:
            return P_DELIM; //symbolize end of expression, we cant parse this token here,
                            //parse what we have in stack already and return control to parser
                            //its end of expr or error, we dont know now
    }

}


/**
 * @desc Push handle '<' after terminal in parameter on stack
 * @desc if terminal dont exist, probably cause inf loop!!!!
 */
int PushHandleAfter(TPrecStack *s, TPrecStackElement *terminal) {
    
    TPrecStackElement *tempElem = NULL;

    TPrecStackElement handleElem; //init of handle start element
    handleElem.next = NULL;
    handleElem.operator = P_HANDLE;
    handleElem.synonym = NULL;

    TPrecStack *tempStack;
    if ((tempStack = (TPrecStack*) malloc(sizeof(TPrecStack))) == NULL )
        return E_INTERNAL;

    InitPStack(tempStack);
    
    while ( (tempElem = TopPStack(s)) != terminal ) { //move nonterminals to tempStack until terminal comes
        
        PushPStack(tempStack, *tempElem);
        PopPStack(s);
    }
    //now push handle <
    PushPStack(s, handleElem);

    //push back old NONTERMs
    while ( (tempElem = TopPStack(tempStack)) != NULL ) {
    	//return stacks data 
        PushPStack(s, *tempElem);
        PopPStack(tempStack);
    }

    DeletePStack(tempStack);

    return E_OK;
}


/**
 * @desc Makes reduction on stack and generate instructions which result is
 * @desc later used as NONTERM
 * @param **synonym imortant, when we finish expr, we will return it to parser (useless in progress)
 */
int MakeReduction(TPrecStack *s, TSynonym **synonym) {
	TPrecStackElement *tempElem = NULL;

    TPrecStackElement data[3]; //space to save elements of reduction

    int result; //to save return value

    for (int i = 0; i < 3; i++) { //init elements
	    data[i].next = NULL;
	    data[i].operator = P_DELIM; //something that we recognize later, $ shouldnt be read typically
	    data[i].synonym = NULL;
    }
    
    for (int i = 2; i >= 0; i--) { //items are on stack in reverse order
        if( ((tempElem = TopPStack(s)) == NULL) || (tempElem->operator == P_HANDLE) ) //end of reduction
            break;
        data[i].operator = tempElem->operator;
        data[i].synonym = tempElem->synonym;
        PopPStack(s);

    }

    //now if isnt P_HANDLE on stack Top mean Syntactic Error (undefined rule)
    if( ((tempElem = TopPStack(s)) == NULL) || (tempElem->operator != P_HANDLE) )
        return E_SYNTACTIC;

    PopPStack(s); //Pop Handle <
    
    //NOW in data data[0] data[1] data[2] are saved items to determite RULE (etc. 0-NONTERM 1-ADD 2-NONTERM)
    int rule; //we need value after cycle
    
    for (rule = 0; rule < RULES_COUNT; rule++) {
    	
        if (data[0].operator == precRules[rule][0] &&      //  etc. E
        	data[1].operator == precRules[rule][1] &&      //       +
        	data[2].operator == precRules[rule][2]    ) {  //       E
            break;
        }
    }

    //we have rule to do in rule

    /////////////////////// INSERT INSTRUCTION PART
    TDataType type = UNDEF_TYPE;
    TDataValue value;
    iInstruction inst = I_STOP;

    switch (rule) { //do instruction specific operation for each
        case R_MUL:
        case R_DIV:
        case R_ADD:
        case R_SUB:
        case R_LESS:
        case R_MORE:
        case R_LESS_EQ:
        case R_MORE_EQ:
        case R_EQ:
        case R_DIFF:
            if( (type = GetResultType(data[0].synonym, data[2].synonym, rule)) == UNDEF_TYPE)
                return E_SEMANTIC_EXP; //wrong data types of operands etc. int + str

            inst = GetInstruction(rule);

		    //Make new synonym for result
		    TSynonym *newSynonym = NewSynonym(sTable, NULL, I_CONST, type, value, false); //synonym to save result
		    
            if (newSynonym == NULL)
		        return E_INTERNAL;
		    
		    if ( (result = InsertInstruction(exprIList, inst, data[0].synonym, data[2].synonym, newSynonym)) != E_OK )
		    	return result;
		    
		    TPrecStackElement newElement;

		    newElement.next = NULL;
		    newElement.operator = P_NONTERM;
		    newElement.synonym = newSynonym;

		    PushPStack(s, newElement);
            break;

        case R_PAR:
            //just push back old NONTERM and we are done
            PushPStack(s, data[1]);
            break;

        case R_NUM:
            //change term to nonterm and push to stack
            data[2].operator = P_NONTERM;
            PushPStack(s, data[2]);
            break;

        case R_ERR: //no such rule, error
        default:
            return E_SYNTACTIC;
    }

    //reduced NONTERM is now on top of stack
    *synonym = TopPStack(s)->synonym; //set stack top synonym as actual result
    return E_OK;
}


/**
 * @desc gets operands of reduction and rule for reduction
 * @desc and determite result data type etc. INT / INT -> REAL, INT + INT -> INT
 */
TDataType GetResultType(TSynonym *operand1, TSynonym *operand2, TRule rule) {
    
    switch (rule) {
    	/////////////////////////////////////////////////////////////////////
        case R_ADD:
            if (operand1->dataType == D_INT && operand2->dataType == D_INT) //int int -> int
                return D_INT;
            else if (operand1->dataType == D_STRING && operand2->dataType == D_STRING) //str str -> str
            	return D_STRING;
            else if ((operand1->dataType == D_INT || operand1->dataType == D_REAL) && //others int real comb -> real
            	     (operand2->dataType == D_INT || operand2->dataType == D_REAL))
            	return D_REAL;
            else
                return UNDEF_TYPE;
    	/////////////////////////////////////////////////////////////////////
        case R_MUL: //int int -> int
        case R_SUB: //others int real comb -> real
            if (operand1->dataType == D_INT && operand2->dataType == D_INT)
                return D_INT;
            else if ((operand1->dataType == D_INT || operand1->dataType == D_REAL) &&
            	     (operand2->dataType == D_INT || operand2->dataType == D_REAL))
            	return D_REAL;
            else
                return UNDEF_TYPE;
        /////////////////////////////////////////////////////////////////////
        case R_DIV: // int || real -> real
            if ((operand1->dataType == D_INT || operand1->dataType == D_REAL) &&
                (operand2->dataType == D_INT || operand2->dataType == D_REAL))
                return D_REAL;
            else
                return UNDEF_TYPE;
        /////////////////////////////////////////////////////////////////////
        case R_LESS:    //
        case R_MORE:    //
        case R_LESS_EQ: // if both int, real, string, boolean -> boolean
        case R_MORE_EQ: //
        case R_EQ:      //
        case R_DIFF:    //
            if ((operand1->dataType == D_INT    && operand2->dataType == D_INT   ) ||
            	(operand1->dataType == D_REAL   && operand2->dataType == D_REAL  ) ||
            	(operand1->dataType == D_STRING && operand2->dataType == D_STRING) ||
            	(operand1->dataType == D_BOOL   && operand2->dataType == D_BOOL  )   )
                return D_BOOL;
            else
                return UNDEF_TYPE;
        /////////////////////////////////////////////////////////////////////
        default:
            return UNDEF_TYPE;
    }
    
}

/**
 * @help to determite what instruction type we will use
 */
iInstruction GetInstruction(int rule) {
    switch (rule) {
        case R_MUL:     return I_MUL;
        case R_DIV:     return I_DIV;
        case R_ADD:     return I_ADD;
        case R_SUB:     return I_SUB;
        case R_LESS:    return I_LESS;
        case R_MORE:    return I_GREATER;
        case R_LESS_EQ: return I_LESS_EQL;
        case R_MORE_EQ: return I_GREATER_EQL;
        case R_EQ:      return I_EQL;
        case R_DIFF:    return I_DIFF;
    }

    return I_STOP;
}


/***********************************************************
                  STACK PART (from stack.c)
************************************************************/

/**
 * @brief Inicializaton of stack
 * @param *stack Pointer to stack
 */
void InitPStack(TPrecStack *stack) {
    stack->top = NULL;
}

/**
 * @brief Push one element to the stackgit 
 * @param *stack Pointer to stack
 * @param Element which should be put into the stack
 * @return True if everything is OK, false if not
 */
bool PushPStack(TPrecStack *stack, TPrecStackElement element) {
    if(stack == NULL) return false;

    TPrecStackElement *newElement;

    newElement = (TPrecStackElement *) malloc(sizeof(TPrecStackElement));
    if(newElement == NULL) return false;

    newElement->next = stack->top;

    stack->top = newElement;

    newElement->operator = element.operator;
    newElement->synonym = element.synonym;

    return true;
}

/**
 * @brief Pop one element from the stack
 * @param *stack Pointer to stack
 * @return True if everything is OK, false if not
 */
bool PopPStack(TPrecStack *stack) {
    if(stack == NULL) return false;

    TPrecStackElement *delElement;

    if(stack->top != NULL) {
        delElement = stack->top;

        stack->top = stack->top->next;

        free(delElement);

        return true;
    }
    return false;
}

/**
 * @brief Read one element from top of the stack
 * @param *stack Pointer to stack
 * @return Pointer to element on top of the stack
 */
TPrecStackElement* TopPStack(TPrecStack *stack) {
    if(stack == NULL) return false;
   
    if(stack->top != NULL) {
        return  stack->top; 
    }
    else {
        return NULL;
    }
}

/**
 * @brief Finds out, if the stack is empty or not
 * @param *stack Pointer to stack
 * @return Tru if empty, false if not   
 */
bool EmptyPStack(TPrecStack *stack) {
    return (stack->top == NULL) ? true : false;
}

/**
 * @brief Deletes the stack
 * @param Pointer to stack
 */
void DeletePStack(TPrecStack *stack) {
    bool tmp = true;
    
    while (tmp != false) {
        tmp = PopPStack(stack);
    }
    free(stack);
}
